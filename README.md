# Eulersolver

This is a project to efficiently solve the compressible Euler equations, a set of non-linear transport equations. Solving this equation in a stable and efficient manner will also give rise to a convergent solver of the Navier-Stokes equations.

## Master thesis
This program was a part of my master thesis. A link to the thesis will be added at a later point. The version of the program at the time of handing in the thesis is marked with the tag "exjobb", with commit hash `7fdeda679ab2273bbca1e4159b0c89aba6ed9b6a`.

## Idea
The main motivation behind this project was to use finite differences in atmospheric modelling. Regular finite difference models are not inherently stable, so the SBP-SAT model has been employed. To increase the effiency of a larger model, one can increase the resolution of areas that are known to require more detail, for example volumes near the ground. The approach taken here is to use a coarse and fine mesh and patch them together with a stable interpolation.

## Building
This project uses CMake
```
mkdir build
cd build
cmake .. -DCMAKE_BUILD_TYPE=Debug -G Ninja
ninja
```

## Testing
To run unit tests, run
```
ninja
ninja test
```

## Configuration
The configurations to each program is determined by `setup.cfg`. This will set the time to run the setup for, boundary conditions, initial conditions and whether to save the output continously or not. See the `testcases` directory for usage.

## Testcases
This directory gives the testcases used for the masters thesis. To recreate the results, run test.py from the appropriate directory. See also the `readme.md` in this directory.

## Limitations
As this is only a research project, there are a couple of limitations

* Two dimensional
* Euler equations
* Boundary conditions are either set by euler vortex or the solution from another grid

None of these limitations are however due to the model used, and can be modified for other problems.


## Dependencies
The project requires

* [libconfig](https://github.com/hyperrealm/libconfig)
