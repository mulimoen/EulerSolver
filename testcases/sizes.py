# Simple script to determine sizes that are suitable for
# testing scaling, and print the corresponding sizes
# of the three grid types (single, double, interpolate)

import numpy as np

steps = 5  # Number of sizes to test
nmin = 25  # Smallest dimensions of grid
nmax = 100  # Largest size of smallest dimension

sizes = np.asarray(np.round(
    10**np.linspace(np.log10(nmin), np.log10(nmax), steps)),
    dtype=int)

print(f"Sizes chosen: {sizes}\n")

print("Resulting dimensions single grid")
for s in sizes:
    print(f'{4*s:3d}x{4*s:3d} ', end='')

print("\n\nResulting dimensions double grid")
for s in sizes:
    print(f'{4*s:3d}x{2*s:3d} ', end='')
print()
for s in sizes:
    print(f'{4*s:3d}x{2*s:3d} ', end='')


print("\n\nResulting dimensions double grid with interpolation")
for s in sizes:
    print(f'{2*s:3d}x{s:3d} ', end='')
print()
for s in sizes:
    print(f'{4*s:3d}x{2*s:3d} ', end='')
print()
