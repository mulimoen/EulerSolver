# This directory contains testcases that test the SBP operators

Testing is divided into three groups

* Convergence
* Stability
* Efficiency

## Convergence
Three subgroups, single domain, split domain, interpolation of two domains. Each of these are split again into the regular operators and the "h/2" operators. To test the other operators, modify "setup.cfg" to use
```
operators = ["4", "4"]
operators = ["9", "9"]
```
Or the "h/2" alternatives
```
operators = ["4", "4 h/2"]
operators = ["9", "9 h/2"]
```

## Stability
Interpolating domains, with the new upwind operators and the traditional operators with no damping. The vortex is weak as to illustrate the instability of the traditonal operators. This is integrated for up to 100 periods

## Efficiency
One case to illustrate how computing time increases with DOF.
Another case illustrates how dividing a domain in two may decrease the error substantially.
