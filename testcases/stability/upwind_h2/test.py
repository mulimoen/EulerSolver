#! /bin/env python3

from helpers import GridGenerator, EulerSolver
import os


def grid_sizes(base_size):
    ny_c = base_size
    nx_c = 2*base_size
    ny_f = 2*base_size
    nx_f = 2*nx_c - 1
    return (nx_f, ny_f, nx_c, ny_c)


def foldername(size):
    return str(size)


def gridgen(size):
    folder = foldername(size)
    if not os.path.isdir(folder):
        os.mkdir(folder)
    setupfile = folder + '/setup.cfg'
    if not os.path.exists(setupfile):
        os.symlink('../setup.cfg', setupfile)

    s = grid_sizes(size)

    GridGenerator(folder + '/grid0.xy',
                  nx=s[0], xbounds=(-5, 5), ny=s[1], ybounds=(-5, 0),
                  shift_y=True)

    GridGenerator(folder + '/grid1.xy',
                  nx=s[2], xbounds=(-5, 5), ny=s[3], ybounds=(0, 5),
                  shift_y=True)


if __name__ == '__main__':
    size = 25
    folder = foldername(size)
    gridgen(size)

    print(f'Calling with sizes {grid_sizes(size)}')
    EulerSolver(folder)
