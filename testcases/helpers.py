from os import path
from subprocess import call, DEVNULL
from tempfile import TemporaryFile


def GridGenerator(filename,
                  nx, xbounds,
                  ny, ybounds,
                  shift_x=False, shift_y=False):

    path_ = path.dirname(path.realpath(__file__)) + \
               "/../build/gridgenerator/gridgenerator"
    arguments = [path_, str(nx), str(ny),
                 str(xbounds[0]), str(xbounds[1]),
                 str(ybounds[0]), str(ybounds[1])]

    if shift_x:
        arguments.append('-SX')
    if shift_y:
        arguments.append('-SY')

    arguments.append(filename)

    call(arguments, stdout=DEVNULL)


def EulerSolver(folder=None, include_timing=False):
    path_ = path.dirname(path.realpath(__file__)) + \
            "/../ReleaseBuild/euler"
    arguments = [path_]
    if folder is not None:
        arguments.extend(["-d", folder])

    if not include_timing:
        ret_val = call(arguments)
        if ret_val:
            raise RuntimeWarning
        return

    with TemporaryFile(mode="w+", encoding="utf-8") as f:
        ret_val = call(arguments, stdout=f)
        if ret_val:
            print("EulerSolver exited with an error")
            raise RuntimeWarning

        f.seek(0)
        text = f.read(-1)

        if ret_val:
            print(text)
            print("EulerSolver exited with an error")
            raise RuntimeWarning

        text = text.split()
        if len(text) != 4:
            raise ValueError("Could not get timing, invalid output")
        timing = float(text[2])
        return timing
