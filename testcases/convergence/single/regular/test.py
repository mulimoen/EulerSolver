#! /bin/env python3

import os
from helpers import GridGenerator, EulerSolver


def foldername(size):
    return str(size)


def gridgen(size):
    folder = foldername(size)

    if not os.path.isdir(folder):
        os.mkdir(folder)
    if not os.path.exists(folder + "/setup.cfg"):
        os.symlink("../setup.cfg", folder + "/setup.cfg")
    GridGenerator(folder + "/grid.xy",
                  nx=4*size, xbounds=(-5, 5),
                  ny=4*size, ybounds=(-5, 5))


if __name__ == '__main__':
    with open('error.txt', 'w') as f:
        sizes = [25, 35, 50, 71, 100]
        for size in sizes:
            folder = foldername(size)
            gridgen(size)

            print(f'Calling with size {4*size}x{4*size}')
            EulerSolver(folder)

            with open(folder + '/error.txt', 'r') as g:
                lines = g.readlines()
                last_line = lines[-1]
            f.write(last_line)
