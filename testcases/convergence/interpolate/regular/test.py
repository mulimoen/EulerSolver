#! /bin/env python3

from helpers import GridGenerator, EulerSolver
import os


def grid_sizes(base_size):
    ny_c = base_size
    nx_c = 2*base_size
    ny_f = 2*base_size
    nx_f = 2*nx_c - 1
    return (nx_f, ny_f, nx_c, ny_c)


def foldername(size):
    return str(size)


def gridgen(size):
    folder = foldername(size)
    if not os.path.isdir(folder):
        os.mkdir(folder)

    setupfile = folder + '/setup.cfg'
    if not os.path.exists(setupfile):
        os.symlink('../setup.cfg', setupfile)

    s = grid_sizes(size)

    GridGenerator(folder + '/grid0.xy',
                  nx=s[0], ny=s[1],
                  xbounds=(-5, 5), ybounds=(-5, 0))
    GridGenerator(folder + '/grid1.xy',
                  nx=s[2], ny=s[3],
                  xbounds=(-5, 5), ybounds=(0, 5))


if __name__ == '__main__':
    with open('error.txt', 'w') as f:
        sizes = [25, 35, 50, 71, 100]
        for size in sizes:
            folder = foldername(size)
            gridgen(size)

            print('Calling with sizes {}x{} + {}x{}'.format(*grid_sizes(size)))
            EulerSolver(folder)

            with open(f'{folder}/error.txt', 'r') as g:
                lines = g.readlines()
                last_line = lines[-1]

            f.write(last_line)
