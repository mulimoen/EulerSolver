#! /bin/env python3

from helpers import GridGenerator, EulerSolver
import os


def foldername(size):
    return str(size)


def gridgen(size):
    folder = foldername(size)
    if not os.path.isdir(folder):
        os.mkdir(folder)

    setupfile = folder + '/setup.cfg'
    if not os.path.exists(setupfile):
        os.symlink('../setup.cfg', setupfile)

    GridGenerator(folder + '/grid0.xy',
                  nx=4*size, ny=2*size,
                  xbounds=(-5, 5), ybounds=(-5, 0))
    GridGenerator(folder + '/grid1.xy',
                  nx=4*size, ny=2*size,
                  xbounds=(-5, 5), ybounds=(0, 5))


if __name__ == '__main__':
    with open('error.txt', 'w') as f:
        sizes = [25, 35, 50, 71, 100]
        for size in sizes:
            folder = foldername(size)
            gridgen(size)

            print(f'Calling with sizes {4*size}x{2*size} x 2')
            EulerSolver(folder)

            with open(f'{folder}/error.txt', 'r') as g:
                lines = g.readlines()
                last_line = lines[-1]

            f.write(last_line)
