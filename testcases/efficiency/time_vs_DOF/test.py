#! /bin/env python3

import os
from helpers import GridGenerator, EulerSolver
from collections import defaultdict
import numpy as np


def foldername(size):
    return str(size)


def gridgen(size):
    folder = foldername(size)

    if not os.path.isdir(folder):
        os.mkdir(folder)
    if not os.path.exists(folder + "/setup.cfg"):
        os.symlink("../setup.cfg", folder + "/setup.cfg")
    GridGenerator(folder + "/grid.xy",
                  nx=size, xbounds=(-5, 5),
                  ny=size, ybounds=(-5, 5),
                  shift_y=True)


if __name__ == '__main__':
    times = defaultdict(list)
    repeats = 5
    sizes = [100, 140, 200, 284, 400]
    for size in sizes:
        for _ in range(repeats):
            folder = foldername(size)
            gridgen(size)

            print(f'Calling with size {size}x{size}')
            time = EulerSolver(folder, include_timing=True)
            print(f"Time: {time} seconds")
            times[size*size].append(time)

    with open('timings.txt', "w") as f:
        f.write("Size, Average Time, Standard Deviation of time\n")
        for (size, time) in times.items():
            av_time = np.average(time)
            std_time = np.std(time)
            f.write(f"{size}, {av_time}, {std_time}\n")
