#! /bin/env python3

import os
from helpers import GridGenerator, EulerSolver


def grid_sizes(base_size):
    ny_1 = base_size
    nx_1 = base_size

    ny_0 = 2*ny_1
    nx_0 = 2*(nx_1 - 1)

    return (nx_0, ny_0, nx_1, ny_1)


def foldername(size):
    return str(size)


def gridgen(size):
    folder = foldername(size)
    if not os.path.isdir(folder):
        os.mkdir(folder)
    setupfile = folder + '/setup.cfg'
    if not os.path.exists(setupfile):
        os.symlink('../setup.cfg', setupfile)

    s = grid_sizes(size)
    GridGenerator(folder + '/grid0.xy', nx=s[0], ny=s[1],
                  xbounds=(-5, 5), ybounds=(0, 10), shift_x=True, shift_y=True)
    GridGenerator(folder + '/grid1.xy', nx=s[2], ny=s[3],
                  xbounds=(-5, 5), ybounds=(10, 20), shift_x=True, shift_y=True)


if __name__ == '__main__':
    with open('error.txt', 'w') as f:
        sizes = [25, 35, 50, 71, 100]
        for size in sizes:
            folder = foldername(size)
            gridgen(size)

            print(f'Calling with sizes {grid_sizes(size)}')
            EulerSolver(folder)

            with open(f'{folder}/error.txt', 'r') as g:
                lines = g.readlines()
                last_line = lines[-1]

            f.write(last_line)
