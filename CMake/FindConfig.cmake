find_package(PkgConfig)
pkg_check_modules(PC_Config QUIET libconfig)

find_path(Config_INCLUDE_DIR
    NAMES libconfig.h
    PATHS ${PC_Config_INCLUDE_DIRS}
)

find_library(Config_LIBRARY_NAMES
    NAMES config
)

set(Config_VERSION "${PC_Config_VERSION}")

mark_as_advanced(Config_INCLUDE_DIR Config_LIBRARY_NAMES Config_VERSION)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(Config
    REQUIRED_VARS Config_INCLUDE_DIR Config_LIBRARY_NAMES
    VERSION_VAR Config_VERSION
)

if (Config_FOUND AND NOT TARGET Config::Config)
    add_library(Config::Config UNKNOWN IMPORTED)
    set_target_properties(Config::Config
        PROPERTIES
            INTERFACE_INCLUDE_DIRECTORIES "${Config_INCLUDE_DIRS}"
            IMPORTED_LINK_INTERFACE_LANGUAGES "C"
            IMPORTED_LOCATION "${Config_LIBRARY_NAMES}"
    )
endif()
