#pragma once

#include "common.h"
#include "metric.h"
#include "operators.h"
#include "vortex.h"


void integration_step(struct fields *result,
                      const struct fields *q0,
                      double *restrict t, double dt,
                      const struct grid *restrict grids, uint32_t n_grids,
                      struct metric *restrict const *restrict metrics,
                      const struct grid_relations *restrict g_rel,
                      const struct SBP_operator op[2],
                      double gamma, struct vortex_parameters v_params);

void integrate(struct fields *result,
               const struct fields *q0,
               double *restrict t, double dt,
               const struct grid *restrict grids, uint32_t n_grids,
               struct metric *restrict const *restrict metrics,
               const struct grid_relations *restrict g_rel,
               const struct SBP_operator op[2],
               double gamma, struct vortex_parameters v_params,
               uint32_t num_steps);

void fluxes(struct fields Ehat, struct fields Fhat,
            const struct fields q,
            const struct metric *restrict metrics,
            const uint32_t N, const double gamma);
