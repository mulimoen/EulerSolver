#pragma once

#include <stdint.h>

struct fields {
    double *restrict rho;
    double *restrict rhou;
    double *restrict rhov;
    double *restrict e;
};

struct grid {
    uint32_t nx;
    uint32_t ny;
    double *x;
    double *y;
};

enum e_border_dir {
    e_border = 0,
    w_border = 1,
    n_border = 2,
    s_border = 3,
};

enum e_border_type {
    border_type_vortex,
    border_type_sat,
    border_type_background,
    border_type_interpolation_to_fine,
    border_type_interpolation_to_coarse
};

struct border_type {
    enum e_border_type tag;
    uint32_t neighbour;
};

struct grid_relations {
    struct border_type border[4];
};
