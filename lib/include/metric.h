#pragma once

#include "common.h"
#include "operators.h"

struct metric {
    double Jxi_x;
    double Jxi_y;
    double Jeta_x;
    double Jeta_y;
    double J;
};

void compute_metrics(struct metric *metrics,
                     uint32_t nxi, uint32_t neta,
                     const double *x, const double *y,
                     const struct diff_op op[2]);
