#pragma once

#include "common.h"


struct vortex_parameters {
    double *x0;
    double *y0;
    double *rstar;
    double *epsilon;
    double Mach;
    uint32_t n_vortices;
    uint32_t _pad;
};

void vortex(struct fields output,
            const double *restrict X, const double *restrict Y,
            double t, uint32_t N,
            struct vortex_parameters params,
            double gamma);


// Can add multiple vortices side by side, make sure vortices are not overlapping
void vortices(struct fields output,
              const double *restrict X, const double *restrict Y,
              const double t, const uint32_t N,
              const struct vortex_parameters params,
              const double gamma);
