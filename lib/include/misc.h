#pragma once

#include "common.h"
#include "operators.h"


void save_solution_to_file(const uint32_t n_grids, const struct grid *grids, const struct fields *solution, const char *filename);

// (q0 - q1)^T H_x (kron) H_y (kron) I_4 (q0 - q1)
// Does not take into account the sizes of the domains,
// errors must be multipled with the area of the domain to be comparable
double h_norm(const struct fields q0, const struct fields q1, const uint32_t nx, const uint32_t ny, const struct h_op op[2]);

void copy_fields(struct fields *copy, const struct fields *original, const struct grid *grids, uint32_t num_grids);
