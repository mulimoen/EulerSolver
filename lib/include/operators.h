#pragma once

#include <stdint.h>

typedef uint32_t operator_type;
#define OPERATOR_TYPE_UNDEFINED ((operator_type) (0u))
#define OPERATOR_TYPE_UPWIND ((operator_type) (1u << 0u))
#define OPERATOR_TYPE_DISSIPATION ((operator_type) (1u << 1u))
#define OPERATOR_TYPE_H ((operator_type) (1u << 2u))
#define OPERATOR_TYPE_H_INVERSE ((operator_type) (1u << 3u))
#define OPERATOR_TYPE_ANTI_SYMMETRIC ((operator_type) (1u << 4u))
#define OPERATOR_TYPE_STAGGERED ((operator_type) (1u << 5u))

typedef void (*spec_diff)(double *restrict /* output */,
                          const double *restrict /* input */,
                          uint32_t /* nxi */, uint32_t /* neta */);
/* Dx = h*op*Q
 * where h is a scaling constant
 *
 * Structure of operator:
 * |-block--|
 * |--------|
 *    |--diag--|
 *       |--diag--|
 *          |--------|
 *          |-block--| // Flipped block (can also be negative if given flag)
 */
struct diff_op {
    const double *block;
    const double *diag;

    const uint32_t cols; // In block
    const uint32_t rows; // In block
    const uint32_t width; // Of stencil

    const operator_type type;

    const spec_diff xi_fn;
    const spec_diff eta_fn;
};

/* A diagonal norm, elements are along the diagonal
 * Not including scaling parameter h
 */
struct h_op {
    const double *block;
    const double diag;
    const uint32_t block_size;
    const operator_type type;
};

/* Interpolation operator
 */
struct interpolation_op {
    const double *block;
    const uint32_t b_rows;
    const uint32_t b_cols;

    const double *diag;
    const uint32_t d_rows;
    const uint32_t d_cols;

    const uint32_t initial_jump;
    const uint32_t jump;
};

struct interpolation_group {
    const struct interpolation_op *F2C;
    const struct interpolation_op *C2F;
};

struct SBP_operator {
    const struct diff_op *diff;
    const struct diff_op *diss;

    const struct h_op *h;
    const struct h_op *hi;

    const struct interpolation_group *interpolation;
};

// These operators assume the layout of the input has xi increasing
// the fastest,
// [V(xi_0, eta_0), V(xi_1, eta_0), V(xi_2, eta_0), ...]
void differentiate_xi(double *restrict output,
                      const double *restrict input,
                      uint32_t nxi, uint32_t neta,
                      struct diff_op op);
void differentiate_eta(double *restrict output,
                       const double *restrict input,
                       uint32_t nxi, uint32_t neta,
                       struct diff_op op);
void interpolate(double *restrict output, uint32_t Noutput,
                 const double *restrict input, uint32_t Ninput,
                 const struct interpolation_op op);

enum e_operator_names {
    e_upwind4,
    e_upwind4_h2,
    e_upwind9,
    e_upwind9_h2,
    e_traditional4,
    e_traditional8
};

void get_operator(struct SBP_operator *op, enum e_operator_names type);
