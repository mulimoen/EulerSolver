#include <math.h>
#include <stdbool.h>
#include <stdio.h>

#include "operators.h"

#include "debug_operator.h"

static bool is_approx_equal(const double x, const double y) {
    const double eps = 1e-6;
    return fabs(x - y) < eps;
}

static bool test_interpolation_operator(const struct interpolation_group op) {
    const uint32_t NC = 101;
    const uint32_t NF = 2*NC - 1;
    double x_c[101];
    double x_f[201]; // 2*NC - 1

    const double h_c = 1.0/(NC - 1.0);
    for (uint32_t i = 0; i < NC; i++) {
        x_c[i] = h_c*i;
    }
    const double h_f = 1.0/(NF - 1.0);
    for (uint32_t i = 0; i < NF; i++) {
        x_f[i] = h_f*i;
    }

    double Ix_f[201];
    interpolate(Ix_f, NF, x_c, NC, *op.C2F);

    for (uint32_t i = 0; i < NC; i++) {
        if (!is_approx_equal(Ix_f[i], x_f[i])) {
            return false;
        }
    }

    double Ix_c[101];
    interpolate(Ix_c, NC, x_f, NF, *op.F2C);
    for (uint32_t i = 0; i < NC; i++) {
        if (!is_approx_equal(Ix_c[i], x_c[i])) {
            return false;
        }
    }

    return true;
}

static bool test_interpolation_operator_h2(const struct interpolation_group op) {
    const uint32_t NC = 101;
    const uint32_t NF = 2*(NC - 1);
    double x_c[101];
    double x_f[200]; // 2*(NC - 1)

    const double h_c = 1.0/(NC - 2.0);
    for (uint32_t i = 1; i < NC-1; i++) {
        x_c[i] = h_c*(i - 0.5);
    }
    x_c[0] = 0;
    x_c[NC-1] = 1.0;

    const double h_f = 1.0/(NF - 2.0);
    for (uint32_t i = 1; i < NF-1; i++) {
        x_f[i] = h_f*(i - 0.5);
    }
    x_f[0] = 0.0;
    x_f[NF-1] = 1.0;

    double Ix_f[201];
    interpolate(Ix_f, NF, x_c, NC, *op.C2F);

    for (uint32_t i = 0; i < NC; i++) {
        if (!is_approx_equal(Ix_f[i], x_f[i])) {
            return false;
        }
    }

    double Ix_c[101];
    interpolate(Ix_c, NC, x_f, NF, *op.F2C);
    for (uint32_t i = 0; i < NC; i++) {
        if (!is_approx_equal(Ix_c[i], x_c[i])) {
            return false;
        }
    }

    return true;
}

static bool test_interpolation_operators(void) {
    bool operators_working = true;

    const double input[12] = {-1, -2, -3, -4, -5, -6, -7, -8, -9, -10, -11, -12};
    const double expected_result[8] = {-55, -130, -230, -302, -330, -434, -390, -140};

    double result[8*2];

    interpolate(result, 8, input, 12, interpolation_debug_op);

    for (uint32_t i = 0; i < 8; i++) {
        if (!is_approx_equal(result[i], expected_result[i])) {
            operators_working = false;
            break;
        }
    }

    struct SBP_operator op;

    get_operator(&op, e_upwind4);
    const bool upwind4_working = test_interpolation_operator(*op.interpolation);
    if (!upwind4_working) {
        fprintf(stderr, "Upwind 4 operator does not interpolate correctly\n");
        operators_working = false;
    }

    get_operator(&op, e_upwind9);
    const bool upwind9_working = test_interpolation_operator(*op.interpolation);
    if (!upwind9_working) {
        fprintf(stderr, "Upwind 9 operator does not interpolate correctly\n");
        operators_working = false;
    }

    get_operator(&op, e_traditional4);
    const bool trad4_working = test_interpolation_operator(*op.interpolation);
    if (!trad4_working) {
        fprintf(stderr, "Traditional 4 operator does not interpolate correctly\n");
        operators_working = false;
    }

    get_operator(&op, e_traditional8);
    const bool trad8_working = test_interpolation_operator(*op.interpolation);
    if (!trad8_working) {
        fprintf(stderr, "Traditional 8 operator does not interpolate correctly\n");
        operators_working = false;
    }

    get_operator(&op, e_upwind9_h2);
    const bool upwind9_h2_working = test_interpolation_operator_h2(*op.interpolation);
    if (!upwind9_h2_working) {
        fprintf(stderr, "Upwind 9 h/2 operator does not interpolate correctly\n");
        operators_working = false;
    }

    return operators_working;
}

static bool test_staggered_difference_operator(const struct diff_op op) {

    const uint32_t N = 101;
    double x[101];
    double dx[101];

    const double h = 1.0/(N - 2);
    x[0] = 0.0;
    x[N-1] = 1.0;

    for (uint32_t i = 1; i < N-1; i++) {
        x[i] = h*(i - 0.5);
    }

    differentiate_xi(dx, x, N, 1, op);
    for (uint32_t i = 0; i < N; i++) {
        if (!is_approx_equal(1.0, dx[i])) {
            return false;
        }
    }
    differentiate_eta(dx, x, 1, N, op);
    for (uint32_t i = 0; i < N; i++) {
        if (!is_approx_equal(1.0, dx[i])) {
            return false;
        }
    }

    return true;
}

static bool test_difference_operator(const struct diff_op op) {
    if (op.type & OPERATOR_TYPE_STAGGERED) {
        return test_staggered_difference_operator(op);
    }

    const uint32_t N = 101;
    double x[101];
    double dx[101];

    const double h = 1.0/(N - 1);
    for (uint32_t i = 0; i < N; i++) {
        x[i] = h*i;
    }

    differentiate_xi(dx, x, N, 1, op);
    for (uint32_t i = 0; i < N; i++) {
        if (!is_approx_equal(1.0, dx[i])) {
            return false;
        }
        dx[i] = 0.0;
    }
    differentiate_eta(dx, x, 1, N, op);
    for (uint32_t i = 0; i < N; i++) {
        if (!is_approx_equal(1.0, dx[i])) {
            return false;
        }
    }

    return true;
}

static bool test_debug_operator() {
    const struct diff_op deb_op = debug_op;

    const double x[4*6] = {
        9, 10, 11, 12, 13, 14,
        9, 10, 11, 12, 13, 14,
        9, 10, 11, 12, 13, 14,
        9, 10, 11, 12, 13, 14,
    };

    double x_res[4*6];

    differentiate_xi(x_res, x, 6, 4, deb_op);

    const double h = 1.0/(6 - 1);

    const double expect[] = {62, 152, 266, 290, 193, 76};
    for (uint32_t i = 0; i < 4; i++) {
        for (uint32_t j = 0; j < 6; j++) {
            if (!is_approx_equal(x_res[6*i + j]*h, expect[j])) {
                return false;
            }
        }
    }

    const double y[4*6] = {
         9,  9,  9,  9,
        10, 10, 10, 10,
        11, 11, 11, 11,
        12, 12, 12, 12,
        13, 13, 13, 13,
        14, 14, 14, 14,
    };

    double y_res[4*6];

    differentiate_eta(y_res, y, 4, 6, deb_op);

    for (uint32_t i = 0; i < 4; i++) {
        for (uint32_t j = 0; j < 6; j++) {
            if (!is_approx_equal(y_res[4*j + i]*h, expect[j])) {
                return false;
            }
        }
    }

    return true;
}


static bool test_difference_operators(void) {
    bool operators_working = true;

    if (!test_debug_operator()) {
        fprintf(stderr, "Debug operator does not give the expected result\n");
        operators_working = false;
    }
    struct SBP_operator op;

    get_operator(&op, e_upwind4);
    if (!test_difference_operator(*op.diff)) {
        fprintf(stderr, "Upwind 4 difference operator not working\n");
        operators_working = false;
    }

    get_operator(&op, e_upwind4_h2);
    if (!test_difference_operator(*op.diff)) {
        fprintf(stderr, "Upwind 4 h/2 difference operator not working\n");
        operators_working = false;
    }

    get_operator(&op, e_upwind9);
    if (!test_difference_operator(*op.diff)) {
        fprintf(stderr, "Upwind 9 difference operator not working\n");
        operators_working = false;
    }

    get_operator(&op, e_upwind9_h2);
    if (!test_difference_operator(*op.diff)) {
        fprintf(stderr, "Upwind 9 h/2 difference operator not working\n");
        operators_working = false;
    }

    get_operator(&op, e_traditional4);
    if (!test_difference_operator(*op.diff)) {
        fprintf(stderr, "Traditional 4 difference operator not working\n");
        operators_working = false;
    }

    get_operator(&op, e_traditional8);
    if (!test_difference_operator(*op.diff)) {
        fprintf(stderr, "Traditional 8 difference operator not working\n");
        operators_working = false;
    }

    return operators_working;
}

int main(void) {
    bool working = true;

    working &= test_difference_operators();

    working &= test_interpolation_operators();

    const bool is_failing = !working;

    return is_failing;
}
