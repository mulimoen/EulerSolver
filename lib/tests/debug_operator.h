#pragma once

#include "operators.h"

static const double debug_op_block[] = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0};
static const double debug_op_diag[] = {7.0, 8.0, 9.0};

static const struct diff_op debug_op = {
    .block = debug_op_block,
    .diag = debug_op_diag,
    .cols = 3,
    .rows = 2,
    .width = 1,
    .type = OPERATOR_TYPE_UNDEFINED,
};

static const double interpolation_debug_op_block[] = {
    1, 2, 3, 4, 5,
    6, 7, 8, 9, 10
};

static const double interpolation_debug_op_diag[] = {
    11, 12, 13, 14,
    15, 16, 17, 18
};

static const struct interpolation_op interpolation_debug_op = {
    .block = interpolation_debug_op_block,
    .b_rows = 2,
    .b_cols = 5,

    .diag = interpolation_debug_op_diag,
    .d_rows = 2,
    .d_cols = 4,

    .initial_jump = 2,
    .jump = 2,
};
