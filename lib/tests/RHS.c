#include <math.h>
#include <stdbool.h>
#include <stdio.h>

#include "RHS.h"

inline static double pressure(const double gamma,
                              const double rho,
                              const double rhou,
                              const double rhov,
                              const double e) {
    return (gamma - 1.0)*(e - (rhou*rhou + rhov*rhov)/(2*rho));
}

static bool test_fluxes(void) {
    // Fluxes is not dependent on direction,
    // it is only necessary to compute at one point to verify

    const double hx = 39.0;
    const double hy = -12;
    const struct metric m = {
        .J = hx*hy,
        .Jxi_x = hx,
        .Jxi_y = 0,
        .Jeta_x = 0,
        .Jeta_y = hy,
    };

    double _rho = 1.0;
    double _rhou = 0.1;
    double _rhov = -0.2;
    double _e = 5.0;
    const struct fields q = {
        .rho = &_rho,
        .rhou = &_rhou,
        .rhov = &_rhov,
        .e = &_e,
    };

    double _0[4] = {0.0};
    struct fields Ehat = {.rho=&_0[0],.rhou=&_0[1],.rhov=&_0[2],.e=&_0[3]};
    double _1[4] = {0.0};
    struct fields Fhat = {.rho=&_1[0],.rhou=&_1[1],.rhov=&_1[2],.e=&_1[3]};
    const double gamma = 1.4;

    fluxes(Ehat, Fhat, q, &m, 1, gamma);

    const double rho = q.rho[0];
    const double rhou = q.rhou[0];
    const double rhov = q.rhov[0];
    const double e = q.e[0];
    const double p = pressure(gamma, rho, rhou, rhov, e);

    bool working = true;
    const double eps = 1e-6;

    working &= fabs(Ehat.rho[0]/m.J*hy - rhou) < eps;
    working &= fabs(Ehat.rhou[0]/m.J*hy - (rhou*rhou/rho + p)) < eps;
    working &= fabs(Ehat.rhov[0]/m.J*hy - rhou*rhov/rho) < eps;
    working &= fabs(Ehat.e[0]/m.J*hy - rhou*(e + p)/rho) < eps;

    working &= fabs(Fhat.rho[0]/m.J*hx - rhov) < eps;
    working &= fabs(Fhat.rhou[0]/m.J*hx - rhou*rhov/rho) < eps;
    working &= fabs(Fhat.rhov[0]/m.J*hx - (rhov*rhov/rho + p)) < eps;
    working &= fabs(Fhat.e[0]/m.J*hx - rhov*(e + p)/rho) < eps;

    return working;
}

int main(void) {
    const bool fluxes_working = test_fluxes();
    if (!fluxes_working) {
        fprintf(stderr, "Computed and analytical fluxes are not equal\n");
    }
    return !fluxes_working;
}
