#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "RHS.h"


struct boundary_data {
    struct fields data; // For SAT, vortex
    enum e_border_type tag;
    uint32_t _pad;
};

struct boundary_conditions {
    struct boundary_data border[4];
};

inline static double pressure(const double gamma,
                              const double rho,
                              const double rhou,
                              const double rhov,
                              const double e) {
    return (gamma - 1.0)*(e - (rhou*rhou + rhov*rhov)/(2*rho));
}


static void add_SAT_penalty(struct fields output,
                            const struct fields q0,
                            const struct fields q1,
                            const uint32_t nxi, const uint32_t neta,
                            const struct h_op hi_op[2],
                            enum e_border_dir side,
                            const struct metric *metrics,
                            const double gamma) {
    assert(metrics);

    const double sign_comp[4] = {
        [n_border] = -1.0,
        [s_border] = 1.0,
        [e_border] = -1.0,
        [w_border] = 1.0,
    };

    const double tau_comp[4] = {
        [n_border] = 1.0,
        [s_border] = -1.0,
        [e_border] = 1.0,
        [w_border] = -1.0,
    };

    // h norm is orthogonal to the side
    const struct h_op op = ((side == w_border) || (side == e_border)) ? hi_op[0] : hi_op[1];

    double hi;
    uint32_t N;
    switch (side) {
        case (w_border) :
            // Fallthrough
        case (e_border) :
            N = neta;
            hi = (op.type & OPERATOR_TYPE_STAGGERED) ? (double)(nxi - 2) : (double)(nxi - 1);
            break;
        case (n_border) :
            // Fallthrough
        case (s_border) :
            N = nxi;
            hi = (op.type & OPERATOR_TYPE_STAGGERED) ? (double)(neta - 2) : (double)(neta - 1);
            break;
    }

    hi *= op.block[0];

    const double tau = tau_comp[side];
    const double sign = sign_comp[side];


    for (uint32_t i = 0; i < N; i++) {
        uint32_t index;
        switch (side) {
            case (w_border) :
            index = nxi*i;
            break;
            case (e_border) :
            index = (nxi - 1) + nxi*i;
            break;
            case (s_border) :
            index = i;
            break;
            case (n_border) :
            index = nxi*(neta-1) + i;
            break;
        }
        const struct metric metric = metrics[index];
        const double rho = q0.rho[index];
        const double rhou = q0.rhou[index];
        const double rhov = q0.rhov[index];
        const double e = q0.e[index];

        double ky_;
        double kx_;
        switch (side) {
        case (n_border) :
            // Fallthrough
        case (s_border) :
            kx_ = metric.Jeta_x/metric.J;
            ky_ = metric.Jeta_y/metric.J;
            break;
        case (w_border) :
            // Fallthrough
        case (e_border) :
            kx_ = metric.Jxi_x/metric.J;
            ky_ = metric.Jxi_y/metric.J;
            break;
        }

        const double kx = kx_/sqrt(kx_*kx_ + ky_*ky_);
        const double ky = ky_/sqrt(kx_*kx_ + ky_*ky_);

        const double u = rhou/rho;
        const double v = rhov/rho;

        const double theta = kx*u + ky*v;

        const double p = pressure(gamma, rho, rhou, rhov, e);
        const double c = sqrt(gamma*p/rho);
        const double phi2 = (gamma - 1.0)*(u*u + v*v)/2.0;

        const double phi2_c2 = (phi2 + c*c)/(gamma - 1.0);

        const double T[4][4] = {
            {               1.0,         0.0,               1.0,               1.0},
            {                 u,          ky,          u + kx*c,          u - kx*c},
            {                 v,         -kx,          v + ky*c,          v - ky*c},
            {phi2/(gamma - 1.0), ky*u - kx*v, phi2_c2 + c*theta, phi2_c2 - c*theta}};

        const double U = kx_*u + ky_*v;
        const double L[4] = {U, U, U + c*sqrt(kx_*kx_ + ky_*ky_), U - c*sqrt(kx_*kx_ + ky_*ky_)};

        const double beta = 1.0/(2.0*c*c);
        const double TI[4][4] = {
            {     1.0 - phi2/(c*c),          (gamma - 1.0)*u/(c*c),          (gamma - 1.0)*v/(c*c), -(gamma - 1.0)/(c*c)},
            {       -(ky*u - kx*v),                             ky,                            -kx,                  0.0},
            {beta*(phi2 - c*theta),  beta*(kx*c - (gamma - 1.0)*u),  beta*(ky*c - (gamma - 1.0)*v),   beta*(gamma - 1.0)},
            {beta*(phi2 + c*theta), -beta*(kx*c + (gamma - 1.0)*u), -beta*(ky*c + (gamma - 1.0)*v),   beta*(gamma - 1.0)}};

        const double res[4] = {
            q0.rho[index] - q1.rho[i],
            q0.rhou[index] - q1.rhou[i],
            q0.rhov[index] - q1.rhov[i],
            q0.e[index] - q1.e[i],
        };


        // TI*res
        double TIres[4];
        for (uint32_t row = 0; row < 4; row++) {
            TIres[row] = 0.0;
            for (uint32_t col = 0; col < 4; col++) {
                TIres[row] += TI[row][col]*res[col];
            }
        }


        // L + sign(abs(L)) * TIres
        double LTIres[4];
        for (uint32_t row = 0; row < 4; row++) {
            LTIres[row] = (L[row] + sign*fabs(L[row]))*TIres[row];
        }

        // T*LTIres
        double TLTIres[4];
        for (uint32_t row = 0; row < 4; row++) {
            TLTIres[row] = 0.0;
            for (uint32_t col = 0; col < 4; col++) {
                TLTIres[row] += T[row][col]*LTIres[col];
            }
        }

        output.rho[index] += hi*tau*TLTIres[0];
        output.rhou[index] += hi*tau*TLTIres[1];
        output.rhov[index] += hi*tau*TLTIres[2];
        output.e[index] += hi*tau*TLTIres[3];
    }
}


void fluxes(struct fields Ehat, struct fields Fhat,
            const struct fields q,
            const struct metric *restrict metrics,
            const uint32_t N, const double gamma) {
    assert(metrics);

//#pragma omp parallel for
    for (uint32_t i = 0; i < N; i++) {
        const double rho = q.rho[i];

        assert(rho > 0.0);

        const double rhou = q.rhou[i];
        const double rhov = q.rhov[i];
        const double e = q.e[i];
        const double p = pressure(gamma, rho, rhou, rhov, e);

        assert(p > 0.0);

        const double E[4] = {
            rhou,
            rhou*rhou/rho + p,
            rhou*rhov/rho,
            rhou*(e + p)/rho,
        };

        const double F[4] = {
            rhov,
            rhou*rhov/rho,
            rhov*rhov/rho + p,
            rhov*(e+p)/rho,
        };

        Ehat.rho[i] = metrics[i].Jxi_x*E[0] + metrics[i].Jxi_y*F[0];
        Fhat.rho[i] = metrics[i].Jeta_x*E[0] + metrics[i].Jeta_y*F[0];

        Ehat.rhou[i] = metrics[i].Jxi_x*E[1] + metrics[i].Jxi_y*F[1];
        Fhat.rhou[i] = metrics[i].Jeta_x*E[1] + metrics[i].Jeta_y*F[1];

        Ehat.rhov[i] = metrics[i].Jxi_x*E[2] + metrics[i].Jxi_y*F[2];
        Fhat.rhov[i] = metrics[i].Jeta_x*E[2] + metrics[i].Jeta_y*F[2];

        Ehat.e[i] = metrics[i].Jxi_x*E[3] + metrics[i].Jxi_y*F[3];
        Fhat.e[i] = metrics[i].Jeta_x*E[3] + metrics[i].Jeta_y*F[3];
    }

}

// scratch should be of size 4*4*N*sizeof(double) bytes
static void upwind_dissipation(struct fields output,
                               const struct metric *restrict metrics,
                               const struct fields q,
                               const uint32_t nxi, const uint32_t neta,
                               const struct diff_op *diss_op[2],
                               const double gamma,
                               double *scratch) {
    assert(metrics);

    const uint32_t N = nxi*neta;

    double *scratch_mem = scratch != NULL ? scratch : malloc(4*4*N*sizeof(double));

    struct fields ad_xi = {
        .rho = &scratch_mem[0*N],
        .rhou = &scratch_mem[1*N],
        .rhov = &scratch_mem[2*N],
        .e = &scratch_mem[3*N],
    };
    struct fields ad_eta = {
        .rho = &scratch_mem[4*N + 0*N],
        .rhou = &scratch_mem[4*N + 1*N],
        .rhov = &scratch_mem[4*N + 2*N],
        .e = &scratch_mem[4*N + 3*N],
    };

//#pragma omp parallel for // Too much memory pressure??
    for (uint32_t i = 0; i < N; i++) {
        const struct metric m = metrics[i];

        const double rho = q.rho[i];
        const double rhou = q.rhou[i];
        const double rhov = q.rhov[i];
        const double e = q.e[i];

        const double u = rhou/rho;
        const double v = rhov/rho;

        const double uhat = m.Jxi_x/m.J*u + m.Jxi_y/m.J*v;
        const double vhat = m.Jeta_x/m.J*u + m.Jeta_y/m.J*v;

        const double p = pressure(gamma, rho, rhou, rhov, e);
        const double c = sqrt(gamma*p/rho);

        const double alpha_u = fabs(uhat) + c;
        const double alpha_v = fabs(vhat) + c;

        ad_xi.rho[i] = alpha_u*m.J*q.rho[i];
        ad_eta.rho[i] = alpha_v*m.J*q.rho[i];

        ad_xi.rhou[i] = alpha_u*m.J*q.rhou[i];
        ad_eta.rhou[i] = alpha_v*m.J*q.rhou[i];

        ad_xi.rhov[i] = alpha_u*m.J*q.rhov[i];
        ad_eta.rhov[i] = alpha_v*m.J*q.rhov[i];

        ad_xi.e[i] = alpha_u*m.J*q.e[i];
        ad_eta.e[i] = alpha_v*m.J*q.e[i];
    }

    struct fields tmp_xi = {
        .rho = &scratch_mem[2*4*N + 0*N],
        .rhou = &scratch_mem[2*4*N + 1*N],
        .rhov = &scratch_mem[2*4*N + 2*N],
        .e = &scratch_mem[2*4*N + 3*N],
    };

    if (diss_op[0] == NULL) {
        for (uint32_t i = 0; i < N; i++) {
            tmp_xi.rho[i] = 0.0;
            tmp_xi.rhou[i] = 0.0;
            tmp_xi.rhov[i] = 0.0;
            tmp_xi.e[i] = 0.0;
        }
    } else {
        differentiate_xi(tmp_xi.rho, ad_xi.rho, nxi, neta, *diss_op[0]);
        differentiate_xi(tmp_xi.rhou, ad_xi.rhou, nxi, neta, *diss_op[0]);
        differentiate_xi(tmp_xi.rhov, ad_xi.rhov, nxi, neta, *diss_op[0]);
        differentiate_xi(tmp_xi.e, ad_xi.e, nxi, neta, *diss_op[0]);
    }

    struct fields tmp_eta = {
        .rho = &scratch_mem[3*4*N + 0*N],
        .rhou = &scratch_mem[3*4*N + 1*N],
        .rhov = &scratch_mem[3*4*N + 2*N],
        .e = &scratch_mem[3*4*N + 3*N],
    };
    if (diss_op[1] == NULL) {
        for (uint32_t i = 0; i < N; i++) {
            tmp_eta.rho[i] = 0.0;
            tmp_eta.rhou[i] = 0.0;
            tmp_eta.rhov[i] = 0.0;
            tmp_eta.e[i] = 0.0;
        }
    } else {
        differentiate_eta(tmp_eta.rho, ad_eta.rho, nxi, neta, *diss_op[1]);
        differentiate_eta(tmp_eta.rhou, ad_eta.rhou, nxi, neta, *diss_op[1]);
        differentiate_eta(tmp_eta.rhov, ad_eta.rhov, nxi, neta, *diss_op[1]);
        differentiate_eta(tmp_eta.e, ad_eta.e, nxi, neta, *diss_op[1]);
    }

    for (uint32_t i = 0; i < N; i++) {
        output.rho[i] = tmp_xi.rho[i] + tmp_eta.rho[i];
        output.rhou[i] = tmp_xi.rhou[i] + tmp_eta.rhou[i];
        output.rhov[i] = tmp_xi.rhov[i] + tmp_eta.rhov[i];
        output.e[i] = tmp_xi.e[i] + tmp_eta.e[i];
    }
    if (!scratch) {
        free(scratch_mem);
    }
}

// Scratch should be of size 7*4*N*sizeof(double) bytes
static void RHS(struct fields result,
                const struct fields q,
                const double t,
                const struct metric *restrict metrics,
                const uint32_t nxi, const uint32_t neta,
                const struct SBP_operator op[2],
                const struct boundary_conditions bnd_cond,
                const double gamma,
                double *scratch) {
    (void) t;

    const uint32_t N = nxi*neta;
    double *scratch_mem = scratch != NULL ? scratch : malloc(4*7*N*sizeof(double));

    struct fields Ehat = {
        .rho = &scratch_mem[2*4*N + 0*N],
        .rhou = &scratch_mem[2*4*N + 1*N],
        .rhov = &scratch_mem[2*4*N + 2*N],
        .e = &scratch_mem[2*4*N + 3*N],
    };
    struct fields Fhat = {
        .rho = &scratch_mem[3*4*N + 0*N],
        .rhou = &scratch_mem[3*4*N + 1*N],
        .rhov = &scratch_mem[3*4*N + 2*N],
        .e = &scratch_mem[3*4*N + 3*N],
    };
    fluxes(Ehat, Fhat, q, metrics, nxi*neta, gamma);

    struct fields dE = {
        .rho = &scratch_mem[0*N],
        .rhou = &scratch_mem[1*N],
        .rhov = &scratch_mem[2*N],
        .e = &scratch_mem[3*N],
    };
    differentiate_xi(dE.rho, Ehat.rho, nxi, neta, *op[0].diff);
    differentiate_xi(dE.rhou, Ehat.rhou, nxi, neta, *op[0].diff);
    differentiate_xi(dE.rhov, Ehat.rhov, nxi, neta, *op[0].diff);
    differentiate_xi(dE.e, Ehat.e, nxi, neta, *op[0].diff);

    struct fields dF = {
        .rho = &scratch_mem[1*4*N + 0*N],
        .rhou = &scratch_mem[1*4*N + 1*N],
        .rhov = &scratch_mem[1*4*N + 2*N],
        .e = &scratch_mem[1*4*N + 3*N],
    };
    differentiate_eta(dF.rho, Fhat.rho, nxi, neta, *op[1].diff);
    differentiate_eta(dF.rhou, Fhat.rhou, nxi, neta, *op[1].diff);
    differentiate_eta(dF.rhov, Fhat.rhov, nxi, neta, *op[1].diff);
    differentiate_eta(dF.e, Fhat.e, nxi, neta, *op[1].diff);

    struct fields ad = {
        .rho = &scratch_mem[2*4*N + 0*N],
        .rhou = &scratch_mem[2*4*N + 1*N],
        .rhov = &scratch_mem[2*4*N + 2*N],
        .e = &scratch_mem[2*4*N + 3*N],
    };
    const struct diff_op *diss[2] = {op[0].diss, op[1].diss};
    upwind_dissipation(ad, metrics, q, nxi, neta, diss, gamma, &scratch_mem[3*4*N]);

    for (uint32_t i = 0; i < nxi*neta; i++) {
        result.rho[i] = (-dE.rho[i] - dF.rho[i] + ad.rho[i])/metrics[i].J;
        result.rhou[i] = (-dE.rhou[i] - dF.rhou[i] + ad.rhou[i])/metrics[i].J;
        result.rhov[i] = (-dE.rhov[i] - dF.rhov[i] + ad.rhov[i])/metrics[i].J;
        result.e[i] = (-dE.e[i] - dF.e[i] + ad.e[i])/metrics[i].J;
    }
    if (!scratch) {
        free(scratch_mem);
    }

    const struct h_op hi[2] = {*op[0].hi, *op[1].hi};

    for (uint32_t c = 0; c < 4; c++) {
        struct fields input = bnd_cond.border[c].data;

        add_SAT_penalty(result, q, input, nxi, neta, hi, c, metrics, gamma);
    }
}

static void malloc_boundary_data(struct boundary_conditions *bnd_cond,
                                 const struct grid_relations *g_rel,
                                 const struct grid *grids,
                                 const uint32_t num_grids) {
    assert(bnd_cond);
    assert(g_rel);
    assert(grids);

    for (uint32_t g = 0; g < num_grids; g++) {
        const uint32_t nxi = grids[g].nx;
        const uint32_t neta = grids[g].ny;
        const uint32_t N[4] = {
            [e_border] = neta,
            [w_border] = neta,
            [n_border] = nxi,
            [s_border] = nxi,
        };

        for (uint32_t c = 0; c < 4; c++) {
            enum e_border_type tag = g_rel[g].border[c].tag;
            bnd_cond[g].border[c].tag = tag;

            double *mem = malloc(4*N[c]*sizeof(double));
            bnd_cond[g].border[c].data.rho = &mem[0*N[c]];
            bnd_cond[g].border[c].data.rhou = &mem[1*N[c]];
            bnd_cond[g].border[c].data.rhov = &mem[2*N[c]];
            bnd_cond[g].border[c].data.e = &mem[3*N[c]];
        }
    }
}

static void free_boundary_data(struct boundary_conditions *bnd_cond, const uint32_t num_grids) {
    assert(bnd_cond);
    for (uint32_t g = 0; g < num_grids; g++) {
        for (uint32_t c = 0; c < 4; c++) {
            free(bnd_cond[g].border[c].data.rho);
        }
    }
}

inline static void north_component(struct fields result, const struct fields from, const struct grid grid) {

    const uint32_t nx = grid.nx;
    const uint32_t ny = grid.ny;

    const size_t offset = nx*(ny - 1);
    memcpy(result.rho, &from.rho[offset], nx*sizeof(double));
    memcpy(result.rhou, &from.rhou[offset], nx*sizeof(double));
    memcpy(result.rhov, &from.rhov[offset], nx*sizeof(double));
    memcpy(result.e, &from.e[offset], nx*sizeof(double));
}

inline static void south_component(struct fields result, const struct fields from, const struct grid grid) {
    const uint32_t nx = grid.nx;

    memcpy(result.rho, from.rho, nx*sizeof(double));
    memcpy(result.rhou, from.rhou, nx*sizeof(double));
    memcpy(result.rhov, from.rhov, nx*sizeof(double));
    memcpy(result.e, from.e, nx*sizeof(double));
}

inline static void west_component(struct fields result, const struct fields from, const struct grid grid) {

    const uint32_t nx = grid.nx;
    const uint32_t ny = grid.ny;

    for (uint32_t j = 0; j < ny; j++) {
        result.rho[j] = from.rho[nx*j];
        result.rhou[j] = from.rhou[nx*j];
        result.rhov[j] = from.rhov[nx*j];
        result.e[j] = from.e[nx*j];
    }
}

inline static void east_component(struct fields result, const struct fields from, const struct grid grid) {

    const uint32_t nx = grid.nx;
    const uint32_t ny = grid.ny;

    for (uint32_t j = 0; j < ny; j++) {
        const uint32_t index = nx*(j+1) - 1;
        result.rho[j] = from.rho[index];
        result.rhou[j] = from.rhou[index];
        result.rhov[j] = from.rhov[index];
        result.e[j] = from.e[index];
    }
}

static void border_component(enum e_border_dir c, struct fields result, const struct fields from, const struct grid grid) {

    switch (c) {
        // Periodic boundaries -> need result from opposite side of other grid
        case(e_border):
            west_component(result, from, grid);
            break;
        case(w_border):
            east_component(result, from, grid);
            break;
        case(n_border):
            south_component(result, from, grid);
            break;
        case(s_border):
            north_component(result, from, grid);
            break;
    }
}


inline static void south_vortex(struct fields result, const struct grid grid, const struct vortex_parameters v_params, const double gamma, const double t) {

    vortices(result, grid.x, grid.y, t, grid.nx, v_params, gamma);
}

inline static void north_vortex(struct fields result, const struct grid grid, const struct vortex_parameters v_params, const double gamma, const double t) {

    const uint32_t offset = grid.nx*(grid.ny - 1);
    vortices(result, &grid.x[offset], &grid.y[offset], t, grid.nx, v_params, gamma);
}

inline static void west_vortex(struct fields result, const struct grid grid, const struct vortex_parameters v_params, const double gamma, const double t, double *scratch) {

    const uint32_t nx = grid.nx;
    const uint32_t ny = grid.ny;

    double *mem = scratch != NULL ? scratch : malloc(2*ny*sizeof(double));

    double *x = &mem[0*ny];
    double *y = &mem[1*ny];
    for (uint32_t i = 0; i < ny; i++) {
        x[i] = grid.x[nx*i];
        y[i] = grid.y[nx*i];
    }

    vortices(result, x, y, t, ny, v_params, gamma);

    if (!scratch) {
        free(mem);
    }
}

inline static void east_vortex(struct fields result, const struct grid grid, const struct vortex_parameters v_params, const double gamma, const double t, double *scratch) {

    const uint32_t nx = grid.nx;
    const uint32_t ny = grid.ny;

    double *mem = scratch != NULL ? scratch : malloc(2*ny*sizeof(double));

    double *x = &mem[0];
    double *y = &mem[ny];
    for (uint32_t i = 0; i < ny; i++) {
        x[i] = grid.x[nx*(i+1)-1];
        y[i] = grid.y[nx*(i+1)-1];
    }

    vortices(result, x, y, t, ny, v_params, gamma);

    if (!scratch) {
        free(mem);
    }
}

static void border_vortex(enum e_border_dir c,
                          struct fields result,
                          const struct grid grid,
                          const struct vortex_parameters v_params,
                          const double gamma, const double t,
                          double *restrict scratch) {

    switch (c) {
        case(w_border) :
            west_vortex(result, grid, v_params, gamma, t, scratch);
            break;
        case(e_border) :
            east_vortex(result, grid, v_params, gamma, t, scratch);
            break;
        case(n_border) :
            north_vortex(result, grid, v_params, gamma, t);
            break;
        case(s_border) :
            south_vortex(result, grid, v_params, gamma, t);
            break;
    }
}

inline static void interpolation_of_north_border(struct fields result, const uint32_t Nout, const struct fields q0, const struct grid grid, const struct interpolation_op *op) {
    assert(op);

    interpolate(result.rho, Nout, &q0.rho[grid.nx*(grid.ny - 1)], grid.nx, *op);
    interpolate(result.rhou, Nout, &q0.rhou[grid.nx*(grid.ny - 1)], grid.nx, *op);
    interpolate(result.rhov, Nout, &q0.rhov[grid.nx*(grid.ny - 1)], grid.nx, *op);
    interpolate(result.e, Nout, &q0.e[grid.nx*(grid.ny - 1)], grid.nx, *op);
}

inline static void interpolation_of_south_border(struct fields result, const uint32_t Nout, const struct fields q0, const struct grid grid, const struct interpolation_op *op) {
    assert(op);

    interpolate(result.rho, Nout, &q0.rho[0], grid.nx, *op);
    interpolate(result.rhou, Nout, &q0.rhou[0], grid.nx, *op);
    interpolate(result.rhov, Nout, &q0.rhov[0], grid.nx, *op);
    interpolate(result.e, Nout, &q0.e[0], grid.nx, *op);
}

inline static void interpolation_of_west_border(struct fields result, const uint32_t Nout, const struct fields q0, const struct grid grid, const struct interpolation_op *op, double *scratch) {
    assert(op);

    double *scratch_mem = scratch != NULL ? scratch : malloc(4*grid.ny*sizeof(double));
    struct fields mem = {
        .rho = &scratch_mem[0*grid.ny],
        .rhou = &scratch_mem[1*grid.ny],
        .rhov = &scratch_mem[2*grid.ny],
        .e = &scratch_mem[3*grid.ny],
    };

    west_component(mem, q0, grid);

    interpolate(result.rho, Nout, mem.rho, grid.ny, *op);
    interpolate(result.rhou, Nout, mem.rhou, grid.ny, *op);
    interpolate(result.rhov, Nout, mem.rhov, grid.ny, *op);
    interpolate(result.e, Nout, mem.e, grid.ny, *op);

    if (!scratch) {
        free(mem.rho);
    }
}

inline static void interpolation_of_east_border(struct fields result, const uint32_t Nout, const struct fields q0, const struct grid grid, const struct interpolation_op *op, double *scratch) {
    assert(op);

    double *scratch_mem = scratch != NULL ? scratch : malloc(4*grid.ny*sizeof(double));
    struct fields mem = {
        .rho = &scratch_mem[0*grid.ny],
        .rhou = &scratch_mem[0*grid.ny],
        .rhov = &scratch_mem[0*grid.ny],
        .e = &scratch_mem[0*grid.ny],
    };

    east_component(mem, q0, grid);

    interpolate(result.rho, Nout, mem.rho, grid.ny, *op);
    interpolate(result.rhou, Nout, mem.rhou, grid.ny, *op);
    interpolate(result.rhov, Nout, mem.rhov, grid.ny, *op);
    interpolate(result.e, Nout, mem.e, grid.ny, *op);

    if (!scratch) {
        free(mem.rho);
    }
}

static void interpolation_border(enum e_border_dir c,
                                 struct fields result, const struct grid this,
                                 const struct fields q0, const struct grid other,
                                 const struct interpolation_op *op,
                                 double *restrict scratch) {
    assert(op);

    switch (c) {
    case(n_border) :
        interpolation_of_south_border(result, this.nx, q0, other, op);
        break;
    case (s_border) :
        interpolation_of_north_border(result, this.nx, q0, other, op);
        break;
    case (w_border) :
        interpolation_of_east_border(result, this.ny, q0, other, op, scratch);
        break;
    case (e_border) :
        interpolation_of_west_border(result, this.ny, q0, other, op, scratch);
    }
}


// Scratch should be of size 4*ny*sizeof(double) (or NULL)
static void set_boundary_data(struct boundary_conditions *bnd_cond,
                              struct fields *q0,
                              const double t,
                              const struct grid_relations *g_rel, const struct grid *grids,
                              const uint32_t num_grids,
                              const struct vortex_parameters v_params,
                              const double gamma,
                              const struct interpolation_group *ops[2],
                              double *scratch) {
    assert(bnd_cond);
    assert(q0);
    assert(g_rel);
    assert(grids);
    assert(ops);

    for (uint32_t g = 0; g < num_grids; g++) {
        for (uint32_t c = 0; c < 4; c++) {
            enum e_border_type tag = bnd_cond[g].border[c].tag;
            uint32_t other_grid;

            struct fields input = bnd_cond[g].border[c].data;

            const struct interpolation_group *interpolation = ((c == e_border) || (c == w_border)) ? ops[1] : ops[0];

            const struct vortex_parameters background = {
                .Mach = v_params.Mach,
                .n_vortices = 0,
            };

            switch (tag) {
                case border_type_vortex:
                    border_vortex(c, input, grids[g], v_params, gamma, t, scratch);
                    break;
                case border_type_sat:
                    other_grid = g_rel[g].border[c].neighbour;
                    border_component(c, input, q0[other_grid], grids[other_grid]);
                    break;
                case border_type_background:
                    border_vortex(c, input, grids[g], background, gamma, t, scratch);
                    break;
                case border_type_interpolation_to_coarse:
                    assert(interpolation);
                    other_grid = g_rel[g].border[c].neighbour;
                    interpolation_border(c, input, grids[g], q0[other_grid], grids[other_grid], interpolation->F2C, NULL);
                    break;
                case border_type_interpolation_to_fine:
                    assert(interpolation);
                    other_grid = g_rel[g].border[c].neighbour;
                    interpolation_border(c, input, grids[g], q0[other_grid], grids[other_grid], interpolation->C2F, NULL);
                    break;
            }
        }
    }
}

void integration_step(struct fields *result,
                      const struct fields *q0,
                      double *restrict t, const double dt,
                      const struct grid *restrict grids, const uint32_t n_grids,
                      struct metric *restrict const *restrict metrics,
                      const struct grid_relations *restrict g_rel,
                      const struct SBP_operator op[2],
                      const double gamma,
                      const struct vortex_parameters v_params) {
    assert(result);
    assert(q0);
    assert(t);
    assert(grids);
    assert(metrics);
    assert(g_rel);
    assert(n_grids);

    const double a[7][6] = {
        {0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
        {4.0/7.0, 0.0, 0.0, 0.0, 0.0, 0.0},
        {115.0/112.0, -5.0/16.0, 0.0, 0.0, 0.0, 0.0},
        {589.0/630.0, 5.0/18.0, -16.0/45.0, 0.0, 0.0, 0.0},
        {229.0/1200.0 - 29.0/6000.0*sqrt(5.0), 119.0/240.0 - 187.0/1200.0*sqrt(5.0), -14.0/75.0 + 34.0/375.0*sqrt(5.0), -3.0/100.0*sqrt(5.0), 0.0, 0.0},
        {71.0/2400.0 - 587.0/12000.0*sqrt(5.0), 187.0/480.0 - 391.0/2400.0*sqrt(5.0), -38.0/75.0 + 26.0/375.0*sqrt(5.0), 27.0/80.0 - 3.0/400.0*sqrt(5.0), (1.0+sqrt(5.0))/4.0, 0.0},
        {-49.0/480.0 + 43.0/160.0*sqrt(5.0), -425.0/96.0 + 51.0/32.0*sqrt(5.0), 52.0/15.0 - 4.0/5.0*sqrt(5.0), -27.0/16.0 + 3.0/16.0*sqrt(5.0), 5.0/4.0 - 3.0/4.0*sqrt(5.0), 5.0/2.0 - 1.0/2.0*sqrt(5.0)},
    };
    const double b[7] = {1.0/12.0, 0.0, 0.0, 0.0, 5.0/12.0, 5.0/12.0, 1.0/12.0};
    const double c[7] = {0.0, 4.0/7.0, 5.0/7.0, 6.0/7.0, (5.0-sqrt(5.0))/10.0, (5.0+sqrt(5.0))/10.0, 1.0};

    struct boundary_conditions *bnd_cond = malloc(n_grids*sizeof(struct boundary_conditions));

    // The interpolation might be NULL if not supported
    const struct interpolation_group *interpolation[2] = {op[0].interpolation, op[1].interpolation};

    malloc_boundary_data(bnd_cond, g_rel, grids, n_grids);

    uint32_t largest_N = 0;
    for (uint32_t g = 0; g < n_grids; g++) {
        const uint32_t N = grids[g].nx*grids[g].ny;
        largest_N = largest_N > N ? largest_N : N;
    }
    double *scratch = malloc(7*4*largest_N*sizeof(double));


    struct fields *QQ = malloc(n_grids*sizeof(struct fields));
    for (uint32_t g = 0; g < n_grids; g++) {
        double *mem = malloc(7*4*grids[g].nx*grids[g].ny*sizeof(double));
        QQ[g].rho = &mem[0*7*grids[g].nx*grids[g].ny];
        QQ[g].rhou = &mem[1*7*grids[g].nx*grids[g].ny];
        QQ[g].rhov = &mem[2*7*grids[g].nx*grids[g].ny];
        QQ[g].e = &mem[3*7*grids[g].nx*grids[g].ny];
    }

    struct fields *q_tmp = malloc(n_grids*sizeof(struct fields));
    for (uint32_t g = 0; g < n_grids; g++) {
        double *mem = malloc(4*grids[g].nx*grids[g].ny*sizeof(double));
        q_tmp[g].rho = &mem[0*grids[g].nx*grids[g].ny];
        q_tmp[g].rhou = &mem[1*grids[g].nx*grids[g].ny];
        q_tmp[g].rhov = &mem[2*grids[g].nx*grids[g].ny];
        q_tmp[g].e = &mem[3*grids[g].nx*grids[g].ny];
    }
    for (uint32_t i = 0; i < 7; i++) {
        for (uint32_t g = 0; g < n_grids; g++) {
            const uint32_t N = grids[g].nx*grids[g].ny;

            // q_tmp = q0
            memcpy(q_tmp[g].rho, q0[g].rho, N*sizeof(double));
            memcpy(q_tmp[g].rhou, q0[g].rhou, N*sizeof(double));
            memcpy(q_tmp[g].rhov, q0[g].rhov, N*sizeof(double));
            memcpy(q_tmp[g].e, q0[g].e, N*sizeof(double));

            for (uint32_t aa = 0; aa < i; aa++) {
                for (uint32_t n = 0; n < N; n++) {
                    q_tmp[g].rho[n] += dt*a[i][aa]*QQ[g].rho[aa*N + n];
                    q_tmp[g].rhou[n] += dt*a[i][aa]*QQ[g].rhou[aa*N + n];
                    q_tmp[g].rhov[n] += dt*a[i][aa]*QQ[g].rhov[aa*N + n];
                    q_tmp[g].e[n] += dt*a[i][aa]*QQ[g].e[aa*N + n];
                }
            }
        }

        const double t_tmp = *t + dt*c[i];

        // Evaluate one iteration
        set_boundary_data(bnd_cond, q_tmp, t_tmp, g_rel, grids, n_grids, v_params, gamma, interpolation, scratch);
        for (uint32_t g = 0; g < n_grids; g++) {
            const uint32_t N = grids[g].nx*grids[g].ny;
            struct fields QQ_offset = {
                .rho = &QQ[g].rho[N*i],
                .rhou = &QQ[g].rhou[N*i],
                .rhov = &QQ[g].rhov[N*i],
                .e = &QQ[g].e[N*i],
            };
            RHS(QQ_offset, q_tmp[g], t_tmp, metrics[g], grids[g].nx, grids[g].ny, op, bnd_cond[g], gamma, scratch);
        }
    }

    for (uint32_t g = 0; g < n_grids; g++) {
        free(q_tmp[g].rho);
    }
    free(q_tmp);

    // Update the solution
    // result = q0
    for (uint32_t g = 0; g < n_grids; g++) {
        const uint32_t N = grids[g].nx*grids[g].ny;
        memcpy(result[g].rho, q0[g].rho, N*sizeof(double));
        memcpy(result[g].rhou, q0[g].rhou, N*sizeof(double));
        memcpy(result[g].rhov, q0[g].rhov, N*sizeof(double));
        memcpy(result[g].e, q0[g].e, N*sizeof(double));
        for (uint32_t j = 0; j < 7; j++) {
            for (uint32_t k = 0; k < N; k++) {
                result[g].rho[k] += dt*b[j]*QQ[g].rho[j*N + k];
                result[g].rhou[k] += dt*b[j]*QQ[g].rhou[j*N + k];
                result[g].rhov[k] += dt*b[j]*QQ[g].rhov[j*N + k];
                result[g].e[k] += dt*b[j]*QQ[g].e[j*N + k];
            }
        }
    }
    *t += dt;

    for (uint32_t g = 0; g < n_grids; g++) {
        free(QQ[g].rho);
    }
    free(QQ);

    free_boundary_data(bnd_cond, n_grids);
    free(scratch);
    free(bnd_cond);
}

void integrate(struct fields *result,
               const struct fields *q0,
               double *restrict t, const double dt,
               const struct grid *restrict grids, const uint32_t n_grids,
               struct metric *restrict const *restrict metrics,
               const struct grid_relations *restrict g_rel,
               const struct SBP_operator op[2],
               const double gamma, const struct vortex_parameters v_params,
               const uint32_t num_steps) {
    assert(result);
    assert(q0);
    assert(t);
    assert(grids);
    assert(metrics);
    assert(g_rel);
    assert(n_grids);
    assert(num_steps);

    const double a[7][6] = {
        {0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
        {4.0/7.0, 0.0, 0.0, 0.0, 0.0, 0.0},
        {115.0/112.0, -5.0/16.0, 0.0, 0.0, 0.0, 0.0},
        {589.0/630.0, 5.0/18.0, -16.0/45.0, 0.0, 0.0, 0.0},
        {229.0/1200.0 - 29.0/6000.0*sqrt(5.0), 119.0/240.0 - 187.0/1200.0*sqrt(5.0), -14.0/75.0 + 34.0/375.0*sqrt(5.0), -3.0/100.0*sqrt(5.0), 0.0, 0.0},
        {71.0/2400.0 - 587.0/12000.0*sqrt(5.0), 187.0/480.0 - 391.0/2400.0*sqrt(5.0), -38.0/75.0 + 26.0/375.0*sqrt(5.0), 27.0/80.0 - 3.0/400.0*sqrt(5.0), (1.0+sqrt(5.0))/4.0, 0.0},
        {-49.0/480.0 + 43.0/160.0*sqrt(5.0), -425.0/96.0 + 51.0/32.0*sqrt(5.0), 52.0/15.0 - 4.0/5.0*sqrt(5.0), -27.0/16.0 + 3.0/16.0*sqrt(5.0), 5.0/4.0 - 3.0/4.0*sqrt(5.0), 5.0/2.0 - 1.0/2.0*sqrt(5.0)},
    };
    const double b[7] = {1.0/12.0, 0.0, 0.0, 0.0, 5.0/12.0, 5.0/12.0, 1.0/12.0};
    const double c[7] = {0.0, 4.0/7.0, 5.0/7.0, 6.0/7.0, (5.0-sqrt(5.0))/10.0, (5.0+sqrt(5.0))/10.0, 1.0};

    struct boundary_conditions *bnd_cond = malloc(n_grids*sizeof(struct boundary_conditions));

    malloc_boundary_data(bnd_cond, g_rel, grids, n_grids);

    // The interpolation might be NULL if not supported
    const struct interpolation_group *interpolation[2] = {op[0].interpolation, op[1].interpolation};

    uint32_t largest_ny = 0;
    for (uint32_t g = 0; g < n_grids; g++) {
        largest_ny = largest_ny > grids[g].ny ? largest_ny : grids[g].ny;
    }
    double *scratch = malloc(4*largest_ny*sizeof(double));

    double **scratch_RHS = malloc(n_grids*sizeof(double*));
    for (uint32_t g = 0; g < n_grids; g++) {
        scratch_RHS[g] = malloc(4*7*grids[g].nx*grids[g].ny*sizeof(double));
    }


    struct fields *QQ = malloc(n_grids*sizeof(struct fields));
    for (uint32_t g = 0; g < n_grids; g++) {
        double *m = malloc(4*7*grids[g].nx*grids[g].ny*sizeof(double));
        QQ[g].rho = &m[0*7*grids[g].nx*grids[g].ny];
        QQ[g].rhou = &m[1*7*grids[g].nx*grids[g].ny];
        QQ[g].rhov = &m[2*7*grids[g].nx*grids[g].ny];
        QQ[g].e = &m[3*7*grids[g].nx*grids[g].ny];
    }

    struct fields *q_tmp = malloc(n_grids*sizeof(struct fields));
    for (uint32_t g = 0; g < n_grids; g++) {
        const uint32_t N = grids[g].nx*grids[g].ny;
        double *mem = malloc(4*N*sizeof(double));
        q_tmp[g] = (struct fields){
            .rho = &mem[0*N],
            .rhou = &mem[1*N],
            .rhov = &mem[2*N],
            .e = &mem[3*N]
        };
        memcpy(result[g].rho, q0[g].rho, N*sizeof(double));
        memcpy(result[g].rhou, q0[g].rhou, N*sizeof(double));
        memcpy(result[g].rhov, q0[g].rhov, N*sizeof(double));
        memcpy(result[g].e, q0[g].e, N*sizeof(double));
    }

    for (uint32_t T_step = 0; T_step < num_steps; T_step++) {
        for (uint32_t i = 0; i < 7; i++) {
            for (uint32_t g = 0; g < n_grids; g++) {
                const uint32_t N = grids[g].nx*grids[g].ny;

                // q_tmp = q0
                memcpy(q_tmp[g].rho, result[g].rho, N*sizeof(double));
                memcpy(q_tmp[g].rhou, result[g].rhou, N*sizeof(double));
                memcpy(q_tmp[g].rhov, result[g].rhov, N*sizeof(double));
                memcpy(q_tmp[g].e, result[g].e, N*sizeof(double));

                for (uint32_t aa = 0; aa < i; aa++) {
                    for (uint32_t n = 0; n < N; n++) {
                        q_tmp[g].rho[n] += dt*a[i][aa]*QQ[g].rho[aa*N + n];
                        q_tmp[g].rhou[n] += dt*a[i][aa]*QQ[g].rhou[aa*N + n];
                        q_tmp[g].rhov[n] += dt*a[i][aa]*QQ[g].rhov[aa*N + n];
                        q_tmp[g].e[n] += dt*a[i][aa]*QQ[g].e[aa*N + n];
                    }
                }
            }

            const double t_tmp = *t + dt*c[i];

            // Evaluate one iteration
            set_boundary_data(bnd_cond, q_tmp, t_tmp, g_rel, grids, n_grids, v_params, gamma, interpolation, scratch);
#pragma omp parallel for schedule(static, 1)
            for (uint32_t g = 0; g < n_grids; g++) {
                const uint32_t N = grids[g].nx*grids[g].ny;
                struct fields QQoffset = {
                    .rho = &QQ[g].rho[N*i],
                    .rhou = &QQ[g].rhou[N*i],
                    .rhov = &QQ[g].rhov[N*i],
                    .e = &QQ[g].e[N*i],
                };
                RHS(QQoffset, q_tmp[g], t_tmp, metrics[g], grids[g].nx, grids[g].ny, op, bnd_cond[g], gamma, scratch_RHS[g]);
            }
        }


        // Update the solution
        for (uint32_t g = 0; g < n_grids; g++) {
            const uint32_t N = grids[g].nx*grids[g].ny;
            for (uint32_t j = 0; j < 7; j++) {
                for (uint32_t k = 0; k < N; k++) {
                    result[g].rho[k] += dt*b[j]*QQ[g].rho[j*N + k];
                    result[g].rhou[k] += dt*b[j]*QQ[g].rhou[j*N + k];
                    result[g].rhov[k] += dt*b[j]*QQ[g].rhov[j*N + k];
                    result[g].e[k] += dt*b[j]*QQ[g].e[j*N + k];
                }
            }
        }
        *t += dt;
    }

    for (uint32_t g = 0; g < n_grids; g++) {
        free(q_tmp[g].rho);
        free(QQ[g].rho);
        free(scratch_RHS[g]);
    }
    free(q_tmp);
    free(QQ);
    free(scratch_RHS);

    free_boundary_data(bnd_cond, n_grids);
    free(bnd_cond);
    free(scratch);
}
