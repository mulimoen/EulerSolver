#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>

#include "operators.h"

#include "operators/upwind4.h"
#include "operators/upwind4_h2.h"
#include "operators/upwind9.h"
#include "operators/upwind9_h2.h"
#include "operators/traditional4.h"
#include "operators/traditional8.h"

void get_operator(struct SBP_operator *op, const enum e_operator_names type) {
    assert(op);
    switch (type) {
        case (e_upwind4) :
            *op = upwind_4;
            break;
        case (e_upwind9) :
            *op = upwind_9;
            break;
        case (e_upwind4_h2) :
            *op = upwind_4_h2;
            break;
        case (e_upwind9_h2) :
            *op = upwind_9_h2;
            break;
        case (e_traditional4):
            *op = traditional_4;
            break;
        case (e_traditional8) :
            *op = traditional_8_interpolator_6;
            break;
    }
}

static void differentiate_xi_one_dimension(
        double *restrict output,
        const double *restrict input,
        const uint32_t nxi,
        const struct diff_op op) {
    assert(input);
    assert(output);

    const double hxi_i = (op.type & OPERATOR_TYPE_STAGGERED) ?
                            (double)(nxi - 2) : (double)(nxi - 1);

    for (uint32_t i = 0; i < op.rows; i++) {
        output[i] = 0;
        for (uint32_t k = 0; k < op.cols; k++) {
            output[i] += input[k]*op.block[op.cols*i + k]*hxi_i;
        }
    }

    const bool anti_symmetric = (op.type & OPERATOR_TYPE_ANTI_SYMMETRIC) == OPERATOR_TYPE_ANTI_SYMMETRIC;
    const double sign = anti_symmetric ? -1.0 : 1.0;

    for (uint32_t i = nxi - op.rows; i < nxi; i++) {
        const uint32_t index = i;
        const uint32_t index_block = nxi - op.cols;

        const uint32_t op_idx = nxi - i;

        output[index] = 0;
        for (uint32_t k = 0; k < op.cols; k++) {
            output[index] += sign*input[index_block+k]*op.block[op.cols*(op_idx) - 1 - k]*hxi_i;
        }
    }

    for (uint32_t i = op.rows; i < nxi - op.rows; i++) {
        const uint32_t index = i;
        output[index] = 0;
        for (uint32_t k = 0; k < 2*op.width + 1; k++) {
            output[index] += op.diag[k]*input[index - op.width + k]*hxi_i;
        }
    }
}

void differentiate_xi(double *restrict output,
                      const double *restrict input,
                      const uint32_t nxi, const uint32_t neta,
                      const struct diff_op op) {
    assert(input);
    assert(output);

    if (op.xi_fn != NULL) {
        (*op.xi_fn)(output, input, nxi, neta);
        return;
    }

    // xi-values are continguous in memory
    for (uint32_t j = 0; j < neta; j++) {
        differentiate_xi_one_dimension(&output[nxi*j], &input[nxi*j], nxi, op);
    }
}

void differentiate_eta(double *restrict output,
                       const double *restrict input,
                       const uint32_t nxi, const uint32_t neta,
                       const struct diff_op op) {
    assert(input);
    assert(output);

    if (op.eta_fn != NULL) {
        (*op.eta_fn)(output, input, nxi, neta);
        return;
    }

    const double heta_i = (op.type & OPERATOR_TYPE_STAGGERED) ?
                            (double)(neta - 2) : (double)(neta - 1);


    for (uint32_t j = 0; j < op.rows; j++) {
        for (uint32_t i = 0; i < nxi; i++) {
            const uint32_t index = j*nxi + i;
            const uint32_t index_block = i;

            output[index] = 0;
            for (uint32_t k = 0; k < op.cols; k++) {
                output[index] += input[index_block + nxi*k]*op.block[op.cols*j + k]*heta_i;
            }
        }
    }

    const bool anti_symmetric = (op.type & OPERATOR_TYPE_ANTI_SYMMETRIC) == OPERATOR_TYPE_ANTI_SYMMETRIC;
    const double sign = anti_symmetric ? -1.0 : 1.0;

    for (uint32_t j = neta - op.rows; j < neta; j++) {
        for (uint32_t i = 0; i < nxi; i++) {
            const uint32_t index = j*nxi + i;
            const uint32_t index_block = nxi*(neta - op.cols) + i;

            const uint32_t op_idx = neta - j;

            output[index] = 0;
            for (uint32_t k = 0; k < op.cols; k++) {
                output[index] += sign*input[index_block + nxi*k]*op.block[op.cols*op_idx - k - 1]*heta_i;
            }
        }
    }

    for (uint32_t j = op.rows; j < neta - op.rows; j++) {
        for (uint32_t i = 0; i < nxi; i++) {
            uint32_t index = j*nxi + i;
            output[index] = 0;
            for (uint32_t k = 0; k < 2*op.width + 1; k++) {
                output[index] += op.diag[k]*input[index - nxi*op.width + nxi*k]*heta_i;
            }
        }
    }
}

void interpolate(double *restrict output, const uint32_t Noutput,
                 const double *restrict input, const uint32_t Ninput,
                 const struct interpolation_op op) {
    assert(output);
    assert(input);
    for (uint32_t i = 0; i < Noutput; i++) {
        output[i] = 0.0;
    }


    for (uint32_t i = 0; i < Noutput; i++) {
        if (i < op.b_rows) { // Block 1
            for (uint32_t k = 0; k < op.b_cols; k++) {
                output[i] += input[k]*op.block[op.b_cols*i + k];
            }
        } else if (Noutput - i <= op.b_rows) { // Block 2
            const uint32_t row = Noutput - i - 1; // Row of interpolation op
            const uint32_t index = Ninput - op.b_cols; // Offset to element to multiply block with

            for (uint32_t k = 0; k < op.b_cols; k++) {
                uint32_t col = op.b_cols - k - 1;
                output[i] += input[index + k]*op.block[op.b_cols*row + col];
            }
        } else { // Middle of the block
            const uint32_t n = i - op.b_rows;

            const uint32_t index = op.initial_jump + op.jump*(n/op.d_rows);

            const uint32_t row = n % op.d_rows;

            for (uint32_t k = 0; k < op.d_cols; k++) {
                output[i] += input[index + k]*op.diag[op.d_cols*row + k];
            }
        }
    }
}
