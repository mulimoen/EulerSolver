#include <assert.h>
#include <stdlib.h>

#include "metric.h"



void compute_metrics(struct metric *metrics, const uint32_t nxi, const uint32_t neta, const double *x, const double *y, const struct diff_op op[2]) {
    assert(metrics);
    assert(x);
    assert(y);

    const uint32_t N = nxi*neta;
    double *mem = malloc(4*N*sizeof(double));

    double *dx_dxi = &mem[0*N];
    differentiate_xi(dx_dxi, x, nxi, neta, op[0]);

    double *dx_deta = &mem[1*N];
    differentiate_eta(dx_deta, x, nxi, neta, op[1]);

    double *dy_dxi = &mem[2*N];
    differentiate_xi(dy_dxi, y, nxi, neta, op[0]);

    double *dy_deta = &mem[3*N];
    differentiate_eta(dy_deta, y, nxi, neta, op[1]);

    for (uint32_t i = 0; i < N; i++) {
        metrics[i].Jxi_x = dy_deta[i];
        metrics[i].Jxi_y = -dx_deta[i];
        metrics[i].Jeta_x = -dy_dxi[i];
        metrics[i].Jeta_y = dx_dxi[i];
        metrics[i].J = dx_dxi[i]*dy_deta[i] - dx_deta[i]*dy_dxi[i];

        assert(metrics[i].J > 0.0);
    }

    free(mem);
}
