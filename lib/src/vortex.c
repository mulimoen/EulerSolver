#include <assert.h>
#include <math.h>

#include "vortex.h"


void vortex(struct fields output, const double *restrict X, const double *restrict Y, const double t, const uint32_t N, const struct vortex_parameters params, const double gamma) {
    assert(X);
    assert(Y);

    const double x0 = *params.x0;
    const double y0 = *params.y0;
    const double rstar = *params.rstar;
    const double eps = *params.epsilon;
    const double pi = atan(1.0)*4.0;
    const double M = params.Mach;
    const double p_inf = 1.0/(gamma*M*M);

    for (uint32_t i = 0; i < N; i++) {
        const double x = X[i];
        const double y = Y[i];

        const double dx = ((x - x0) - t);
        const double dy = y - y0;
        const double f = (1.0 - (dx*dx + dy*dy))/(rstar*rstar);

        const double u = 1.0 - eps*dy/(2.0*pi*sqrt(p_inf)*rstar*rstar)*exp(f/2.0);
        const double v =       eps*dx/(2.0*pi*sqrt(p_inf)*rstar*rstar)*exp(f/2.0);
        const double rho = pow(1 - eps*eps*(gamma - 1.0)*M*M/(8.0*pi*pi*p_inf*rstar*rstar)*exp(f), 1.0/(gamma - 1.0));
        const double p = pow(rho, gamma)*p_inf;

        const double e = p/(gamma - 1.0) + rho*(u*u + v*v)/2.0;

        output.rho[i] = rho;
        output.rhou[i] = rho*u;
        output.rhov[i] = rho*v;
        output.e[i] = e;
    }
}

static void background_stream(struct fields output, const uint32_t N, const double Mach, const double gamma) {
    const double M = Mach;
    const double p_inf = 1.0/(gamma*M*M);

    const double rho = 1;
    const double u = 1;
    const double v = 0;
    const double e = p_inf/(gamma - 1.0) + rho*(u*u + v*v)/2.0;

    for (uint32_t i = 0; i < N; i++) {
        output.rho[i] = rho;
        output.rhou[i] = rho*u;
        output.rhov[i] = rho*v;
        output.e[i] = e;
     }
}

static void vortice(struct fields output, const double *restrict X, const double *restrict Y, const double t, const uint32_t N, const double x0, const double y0, const double rstar, const double eps, const double Mach, const double gamma) {
    assert(X);
    assert(Y);

    const double M = Mach;
    const double pi = atan(1.0)*4.0;
    const double p_inf = 1.0/(gamma*M*M);

    const double rho_free = 1;
    const double u_free = 1;
    const double v_free = 0;
    const double rhou_free = rho_free*u_free;
    const double rhov_free = rho_free*v_free;
    const double e_free = p_inf/(gamma - 1.0) + rho_free*(u_free*u_free + v_free*v_free)/2.0;

    for (uint32_t i = 0; i < N; i++) {
        const double x = X[i];
        const double y = Y[i];

        const double dx = ((x - x0) - t);
        const double dy = y - y0;
        const double f = (1.0 - (dx*dx + dy*dy))/(rstar*rstar);

        const double u = 1.0 - eps*dy/(2.0*pi*sqrt(p_inf)*rstar*rstar)*exp(f/2.0);
        const double v =       eps*dx/(2.0*pi*sqrt(p_inf)*rstar*rstar)*exp(f/2.0);
        const double rho = pow(1 - eps*eps*(gamma - 1.0)*M*M/(8.0*pi*pi*p_inf*rstar*rstar)*exp(f), 1.0/(gamma - 1.0));
        const double p = pow(rho, gamma)*p_inf;

        const double e = p/(gamma - 1.0) + rho*(u*u + v*v)/2.0;

        output.rho[i] += rho - rho_free;
        output.rhou[i] += rho*u - rhou_free;
        output.rhov[i] += rho*v - rhov_free;
        output.e[i] += e - e_free;
    }
}

void vortices(struct fields output, const double *restrict X, const double *restrict Y, const double t, const uint32_t N, const struct vortex_parameters params, const double gamma) {
    assert(X);
    assert(Y);

    background_stream(output, N, params.Mach, gamma);
    for (uint32_t i = 0; i < params.n_vortices; i++) {
        vortice(output, X, Y, t, N, params.x0[i], params.y0[i], params.rstar[i], params.epsilon[i], params.Mach, gamma);
    }
}
