#pragma once

// Forgive me for these horrible macros
// This does way to much stuff to create something which the
// compiler should be able to inline, but does not.


// Creates a function
// static void name(
//      double *restrict input,
//      const double *restrict output,
//      const double nxi, const double neta)
// Which can be used to instantiate a specialised and inlined
// variant of the xi function
// Also creates a name_1D (internal) helper function
//
// DIFF_XI_SPECIALISATION takes the following argument
// name: name of the function
// diff_block: doubles describing the block
// nblock_x: size of the diff_block in x
// nblock_y: size of the diff_block in y
// sign_second_block: + or -, - for antisymmetric block
// diff_diag: doubles describing the diagonal component
// ndiag: size of diff_diag
// is_shifted: determines whether to use the alternative h
#define DIFF_XI_SPECIALISATION(name, \
        diff_block, nblock_x, nblock_y, sign_second_block, \
        diff_diag, ndiag, is_shifted) \
    inline static void name##_1D ( \
            double *restrict output, \
            const double *restrict input, \
            const uint32_t nxi) { \
        const double hxi_i = is_shifted ? nxi - 2 : nxi - 1; \
        for (uint32_t i = 0; i < nblock_y; i++) { /* First block */ \
            const double *block = &diff_block[nblock_x*i]; \
            output[i] = 0; \
            for (uint32_t j = 0; j < nblock_x; j++) { \
                output[i] += hxi_i*block[j]*input[j]; \
            } \
        } \
        for (uint32_t i = nblock_y; i < nxi - nblock_y; i++) { /* Diagonal part */ \
            const double *diag = diff_diag; \
            output[i] = 0; \
            for (uint32_t j = 0; j < ndiag; j++) { \
                output[i] += hxi_i*diag[j]*input[i - ndiag/2 + j]; \
            } \
        } \
        for (uint32_t i = 0; i < nblock_y; i++) { /* Last block */ \
            output[nxi - 1 - i] = 0; \
            for (uint32_t j = 0; j < nblock_x; j++) { \
                const double *block = &diff_block[nblock_x*i]; \
                output[nxi-1-i] sign_second_block##= hxi_i*input[nxi-1-j]*block[j]; \
            } \
        } \
    } \
    static void name ( \
            double *restrict output, \
            const double *restrict input, \
            const uint32_t nxi, const uint32_t neta) { \
        for (uint32_t i = 0; i < neta; i++) { \
            name##_1D(&output[nxi*i], &input[nxi*i], nxi); \
        } \
    }


// Creates a function
// static void name(
//      double *restrict input,
//      const double *restrict output,
//      const double nxi, const double neta)
// Which can be used to instantiate a specialised and inlined
// variant of the eta function
//
// DIFF_ETA_SPECIALISATION takes the following argument
// name: name of the function
// diff_block: doubles describing the block
// nblock_x: size of the diff_block in x
// nblock_y: size of the diff_block in y
// sign_second_block: + or -, - for antisymmetric block
// diff_diag: doubles describing the diagonal component
// ndiag: size of diff_diag
// is_shifted: determines whether to use the alternative h
#define DIFF_ETA_SPECIALISATION(name, \
        diff_block, nblock_x, nblock_y, sign_second_block, \
        diff_diag, ndiag, is_shifted) \
    static void name ( \
            double *restrict output, \
            const double *restrict input, \
            const uint32_t nxi, const uint32_t neta) { \
        const double heta_i = is_shifted ? neta - 2 : neta - 1; \
        for (uint32_t i = 0; i < nxi; i++) { \
            const double *in = &input[i]; \
            double *out = &output[i]; \
            for (uint32_t j = 0; j < nblock_y; j++) { /* First block */ \
                const double *block = &diff_block[nblock_x*j]; \
                out[j*nxi] = 0; \
                for (uint32_t k = 0; k < nblock_x; k++) { \
                    out[j*nxi] += heta_i*(in[k*nxi]*block[k]); \
                } \
            } \
            for (uint32_t j = nblock_y; j < neta - nblock_y; j++) { /* Diag */ \
                out[nxi*j] = 0; \
                for (uint32_t k = 0; k < ndiag; k++) { \
                    out[nxi*j] += heta_i*in[nxi*(j+k-ndiag/2)]*diff_diag[k]; \
                } \
            } \
            for (uint32_t j = 0; j < nblock_y; j++) { /* Last block */ \
                out[nxi*(neta-1-j)] = 0; \
                const double *block = &diff_block[nblock_x*j]; \
                for (uint32_t k = 0; k < nblock_x; k++) { \
                    out[nxi*(neta-1-j)] sign_second_block##= heta_i*in[nxi*(neta-1-k)]*block[k]; \
                } \
            } \
        } \
    }

// Creates two function
// static void name_{xi,eta}(
//      double *restrict input,
//      const double *restrict output,
//      const double nxi, const double neta)
// Which can be used to instantiate a specialised and inlined
// variant of the {xi,eta} functions
// Also creates a name_1D (internal) helper function


// DIFF_OP_SPECIALISATIONS takes the following argument
// name: name of the functions according to above
// diff_block: doubles describing the block
// nblock_x: size of the diff_block in x
// nblock_y: size of the diff_block in y
// sign_second_block: + or -, - for antisymmetric block
// diff_diag: doubles describing the diagonal component
// ndiag: size of diff_diag
// is_shifted: determines whether to use the alternative h
#define DIFF_OP_SPECIALISATIONS(name, \
        diff_block, nblock_x, nblock_y, sign_second_block, \
        diff_diag, ndiag, is_shifted) \
    DIFF_XI_SPECIALISATION(name##_xi, \
        diff_block, nblock_x, nblock_y, sign_second_block, \
        diff_diag, ndiag, is_shifted) \
    DIFF_ETA_SPECIALISATION(name##_eta, \
        diff_block, nblock_x, nblock_y, sign_second_block, \
        diff_diag, ndiag, is_shifted)
