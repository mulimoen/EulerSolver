#pragma once

#include "operators.h"
#include "macro.h"

static const double upwind_4_h2_diff_block[] = {
    -3.95604395604396e+00, 5.41758241758242e+00, -1.94505494505495e+00, 4.83516483516484e-01, 0.00000000000000e+00, 0.00000000000000e+00, 0.00000000000000e+00,
    -8.09025641025641e-01, 0.00000000000000e+00, 1.03948717948718e+00, -2.47384615384615e-01, 1.69230769230769e-02, 0.00000000000000e+00, 0.00000000000000e+00,
    2.37983193277311e-01, -8.51680672268908e-01, 0.00000000000000e+00, 7.35966386554622e-01, -1.36134453781513e-01, 1.38655462184874e-02, 0.00000000000000e+00,
    -6.14632442814737e-02, 2.10581456259822e-01, -7.64623712240265e-01, 0.00000000000000e+00, 7.42535358826611e-01, -1.41435306443164e-01, 1.44054478784704e-02,
};

static const double upwind_4_h2_diff_diag[] = {
    -1.43229166666667e-02, 1.40625000000000e-01, -7.38281250000000e-01, 0.00000000000000e+00, 7.38281250000000e-01, -1.40625000000000e-01, 1.43229166666667e-02
};

DIFF_OP_SPECIALISATIONS(upwind_4_h2_diff,
        upwind_4_h2_diff_block, 7, 4, -,
        upwind_4_h2_diff_diag, 7, true)

static const struct diff_op upwind_4_h2_diff = {
    .block = upwind_4_h2_diff_block,
    .diag = upwind_4_h2_diff_diag,
    .cols = 7,
    .rows = 4,
    .width = 3,
    .type = OPERATOR_TYPE_UPWIND | OPERATOR_TYPE_ANTI_SYMMETRIC | OPERATOR_TYPE_STAGGERED,

    .xi_fn = upwind_4_h2_diff_xi,
    .eta_fn = upwind_4_h2_diff_eta,
};

static const double upwind_4_h2_diss_block[] = {
    -2.76989342315976e-01, 5.19355016842454e-01, -3.46236677894969e-01, 1.03871003368491e-01, 0.00000000000000e+00, 0.00000000000000e+00, 0.00000000000000e+00,
     7.75570158484731e-02, -1.62342481638964e-01, 1.47715500579822e-01, -7.98531117124082e-02, 1.69230769230769e-02, 0.00000000000000e+00, 0.00000000000000e+00,
     -4.23630758836198e-02, 1.21027405937249e-01, -1.91609307039399e-01, 1.82272708078206e-01, -8.31932773109244e-02, 1.38655462184874e-02, 0.00000000000000e+00,
     1.32037874021759e-02, -6.79734450144910e-02, 1.89370108794365e-01, -2.78654929966754e-01, 2.16081718177056e-01, -8.64326872708224e-02, 1.44054478784704e-02,
};

static const double upwind_4_h2_diss_diag[] = {
    1.43229166666667e-02, -8.59375000000000e-02, 2.14843750000000e-01, -2.86458333333333e-01, 2.14843750000000e-01, -8.59375000000000e-02, 1.43229166666667e-02,
};

DIFF_OP_SPECIALISATIONS(upwind_4_h2_diss,
        upwind_4_h2_diss_block, 7, 4, +,
        upwind_4_h2_diss_diag, 7, true)

static const struct diff_op upwind_4_h2_diss = {
    .block = upwind_4_h2_diss_block,
    .diag = upwind_4_h2_diss_diag,
    .cols = 7,
    .rows = 4,
    .width = 3,
    .type = OPERATOR_TYPE_UPWIND | OPERATOR_TYPE_DISSIPATION | OPERATOR_TYPE_STAGGERED,

    .xi_fn = upwind_4_h2_diss_xi,
    .eta_fn = upwind_4_h2_diss_eta,
};

static const double upwind_4_h2_h_block[] = {
    0.91e2/0.720e3, 0.325e3/0.384e3, 0.595e3/0.576e3, 0.1909e4/0.1920e4,
};

static const struct h_op upwind_4_h2_h = {
    .block = upwind_4_h2_h_block,
    .diag = 1.0,
    .block_size = 4,
    .type = OPERATOR_TYPE_H | OPERATOR_TYPE_STAGGERED,
};

static const double upwind_4_h2_hi_block[] = {
    0.720e3/0.91e2, 0.384e3/0.325e3, 0.576e3/0.595e3, 0.1920e4/0.1909e4,
};

static const struct h_op upwind_4_h2_hi = {
    .block = upwind_4_h2_hi_block,
    .diag = 1.0,
    .block_size = 4,
    .type = OPERATOR_TYPE_H_INVERSE | OPERATOR_TYPE_STAGGERED,
};

static const struct SBP_operator upwind_4_h2 = {
    .diff = &upwind_4_h2_diff,
    .diss = &upwind_4_h2_diss,

    .h = &upwind_4_h2_h,
    .hi = &upwind_4_h2_hi,
    .interpolation = NULL,
};
