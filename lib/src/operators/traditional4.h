#pragma once

#include "operators.h"
#include "macro.h"


static const double traditional_4_diff_block[] = {
    -1.41176470588235e+00, 1.73529411764706e+00, -2.35294117647059e-01, -8.82352941176471e-02, 0.00000000000000e+00, 0.00000000000000e+00,
    -5.00000000000000e-01, 0.00000000000000e+00, 5.00000000000000e-01, 0.00000000000000e+00, 0.00000000000000e+00, 0.00000000000000e+00,
    9.30232558139535e-02, -6.86046511627907e-01, 0.00000000000000e+00, 6.86046511627907e-01, -9.30232558139535e-02, 0.00000000000000e+00,
    3.06122448979592e-02, 0.00000000000000e+00, -6.02040816326531e-01, 0.00000000000000e+00, 6.53061224489796e-01, -8.16326530612245e-02,
};

static const double traditional_4_diff_diag[] = {
    1.0/12.0, -2.0/3.0, 0.0, 2.0/3.0, -1.0/12.0,
};

DIFF_OP_SPECIALISATIONS(traditional_4_diff,
        traditional_4_diff_block, 6, 4, -,
        traditional_4_diff_diag, 5, false)

static const struct diff_op traditional_4_diff = {
    .block = traditional_4_diff_block,
    .diag = traditional_4_diff_diag,
    .cols = 6,
    .rows = 4,
    .width = 2,
    .type = OPERATOR_TYPE_ANTI_SYMMETRIC,

    .xi_fn = traditional_4_diff_xi,
    .eta_fn = traditional_4_diff_eta,
};

static const double traditional_4_h_block[] = {
    17.0/48.0, 59.0/48.0, 43.0/48.0, 49.0/48.0,
};

static const struct h_op traditional_4_h = {
    .block = traditional_4_h_block,
    .diag = 1.0,
    .block_size = 4,
    .type = OPERATOR_TYPE_H,
};

static const double traditional_4_hi_block[] = {
    48.0/17.0, 48.0/59.0, 48.0/43.0, 48.0/49.0,
};

static const struct h_op traditional_4_hi = {
    .block = traditional_4_hi_block,
    .diag = 1.0,
    .block_size = 4,
    .type = OPERATOR_TYPE_H_INVERSE,
};

#include "upwind4.h"
static const struct SBP_operator traditional_4 = {
    .diff = &traditional_4_diff,
    .diss = NULL,

    .h = &traditional_4_h,
    .hi = &traditional_4_hi,

    .interpolation = &upwind_4_interpolation_group,
};
