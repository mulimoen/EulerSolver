#include <assert.h>
#include <errno.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "misc.h"

void save_solution_to_file(const uint32_t n_grids, const struct grid *grids, const struct fields *solution, const char *filename) {
    errno = 0;
    FILE *file = fopen(filename, "w+e");
    if (file == 0) {
        fprintf(stderr, "Could not open file for writing: %s\n", strerror(errno));
        return;
    }

    fwrite(&n_grids, sizeof(n_grids), 1, file);
    for (uint32_t g = 0; g < n_grids; g++) {
        const uint32_t N[2] = {grids[g].nx, grids[g].ny};
        fwrite(N, sizeof(N), 1, file);

        fwrite(grids[g].x, N[0]*N[1], sizeof(double), file);
        fwrite(grids[g].y, N[0]*N[1], sizeof(double), file);

        fwrite(solution[g].rho, N[0]*N[1], sizeof(double), file);
        fwrite(solution[g].rhou, N[0]*N[1], sizeof(double), file);
        fwrite(solution[g].rhov, N[0]*N[1], sizeof(double), file);
        fwrite(solution[g].e, N[0]*N[1], sizeof(double), file);
    }

    fclose(file);
}

double h_norm(const struct fields q0, const struct fields q1, const uint32_t nx, const uint32_t ny, const struct h_op op[2]) {
    assert(ny);

    double *collapse = malloc(ny*sizeof(double));
    for (uint32_t i = 0; i < ny; i++) {
        collapse[i] = 0.0;
    }

    const double hx = (op[0].type & OPERATOR_TYPE_STAGGERED) ?
        1.0/(double)(nx - 2) : 1.0/(double)(nx - 1);

    for (uint32_t j = 0; j < ny; j++) {
        for (uint32_t i = 0; i < nx; i++) {
            double hi;
            const uint32_t ni = ny - i - 1;
            if (i < op[0].block_size) {
                hi = op[0].block[i];
            } else if (ni < op[0].block_size) {
                hi = op[0].block[ni];
            } else {
                hi = op[0].diag;
            }
            hi *= hx;

            const double diff_rho = q0.rho[nx*j + i] - q1.rho[nx*j + i];
            collapse[j] += diff_rho*diff_rho*hi;

            const double diff_rhou = q0.rhou[nx*j + i] - q1.rhou[nx*j + i];
            collapse[j] += diff_rhou*diff_rhou*hi;

            const double diff_rhov = q0.rhov[nx*j + i] - q1.rhov[nx*j + i];
            collapse[j] += diff_rhov*diff_rhov*hi;

            const double diff_e = q0.e[nx*j + i] - q1.e[nx*j + i];
            collapse[j] += diff_e*diff_e*hi;
        }
    }

    double err = 0.0;

    const double hy = (op[1].type & OPERATOR_TYPE_STAGGERED) ?
        1.0/(double)(ny - 2) : 1.0/(double)(ny - 1);

    for (uint32_t j = 0; j < ny; j++) {
        double hj;
        const uint32_t nj = ny - j - 1;
        if (j < op[1].block_size) {
            hj = op[1].block[j];
        } else if (nj < op[1].block_size) {
            hj = op[1].block[nj];
        } else {
            hj = op[1].diag;
        }
        hj *= hy;

        err += collapse[j]*hj;
    }

    free(collapse);
    return sqrt(err);
}

void copy_fields(struct fields *copy, const struct fields *original, const struct grid *grids, const uint32_t num_grids) {
    assert(copy);
    assert(original);
    assert(grids);

    for (uint32_t g = 0; g < num_grids; g++) {
        const uint32_t N = grids[g].nx*grids[g].ny;
        for (uint32_t i = 0; i < N; i++) {
            copy[g].rho[i] = original[g].rho[i];
            copy[g].rhou[i] = original[g].rhou[i];
            copy[g].rhov[i] = original[g].rhov[i];
            copy[g].e[i] = original[g].e[i];
        }
    }
}
