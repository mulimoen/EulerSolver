#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

int main(int argc, char **argv) {
    const char *default_name = "grid.xy";
    bool staggered_X = false;
    bool staggered_Y = false;
    if (argc < 7) {
        printf("Usage: %s nx ny x0 xN y0 yN [output] [staggered_x] [staggered_y]\n", argv[0]);
        printf("output is optional, default value is \"%s\"\n", default_name);
        printf("staggered_x is optional, pass -SX to set to true\n");
        printf("staggered_y is optional, pass -SY to set to true\n");
        return EXIT_SUCCESS;
    }

    const char *filename = default_name;

    for (int pot = 7; pot < argc; pot++) {
        if (strcmp(argv[pot], "-SX") == 0) {
            staggered_X = true;
        } else if (strcmp(argv[pot], "-SY") == 0) {
            staggered_Y = true;
        } else {
            filename = argv[pot];
        }
    }

    const long long int nx_ = strtoll(argv[1], NULL, 10);
    if (nx_ < 0 || nx_ > UINT32_MAX) {
        fprintf(stderr, "nx has an invalid size: %lld\n", nx_);
        return EXIT_FAILURE;
    }
    const uint32_t nx = (uint32_t)nx_;

    const long long int ny_ = strtoll(argv[2], NULL, 10);
    if (ny_ < 0 || ny_ > UINT32_MAX) {
        fprintf(stderr, "ny has an invalid size: %lld\n", ny_);
        return EXIT_FAILURE;
    }
    const uint32_t ny = (uint32_t)ny_;

    const double x0 = strtod(argv[3], NULL);
    const double xN = strtod(argv[4], NULL);
    const double y0 = strtod(argv[5], NULL);
    const double yN = strtod(argv[6], NULL);


    printf("Format of grid: (x, y)\n");
    printf("(%4.4f, %4.4f) -------- (%4.4f, %4.4f)\n", x0, yN, xN, yN);
    printf("       |                         |  \n");
    printf("       |                         |  \n");
    printf("       |                         |  \n");
    printf("       |                         |  \n");
    printf("(%4.4f, %4.4f) -------- (%4.4f, %4.4f)\n", x0, y0, xN, y0);
    printf("With size (ny x nx) : %d x %d\n", ny, nx);
    if (staggered_X) {
        printf("Grid is staggered in X\n");
    }
    if (staggered_Y) {
        printf("Grid is staggered in Y\n");
    }

    FILE *file = fopen(filename, "w+");
    if (!file) {
        fprintf(stderr, "Could not open \"%s\" for writing\n", filename);
        return EXIT_FAILURE;
    }

    printf("Saving to file \"%s\"\n", filename);

    double *mem = malloc(2*nx*ny*sizeof(double));
    if (!mem) {
        fprintf(stderr, "Memory allocation error\n");
        return EXIT_FAILURE;
    }
    double *X = &mem[0];
    double *Y = &mem[nx*ny];

    if (staggered_X) {
        const double hx = (xN - x0)/(nx - 2.0);
        for (uint32_t j = 0; j < ny; j++) {
            X[nx*j + 0] = x0;
            X[nx*j + nx - 1] = xN;
            for (uint32_t i = 1; i < nx-1; i++) {
                X[nx*j + i] = x0 + hx*(i - 0.5);
            }
        }
    } else {
        const double hx = (xN - x0)/(nx - 1.0);
        for (uint32_t j = 0; j < ny; j++) {
            for (uint32_t i = 0; i < nx; i++) {
                X[nx*j + i] = x0 + hx*i;
            }
        }
    }
    if (staggered_Y) {
        const double hy = (yN - y0)/(ny - 2.0);
        for (uint32_t i = 0; i < nx; i++) {
            Y[i] = y0;
            Y[nx*(ny- 1) + i] = yN;
            for (uint32_t j = 1; j < ny-1; j++) {
                Y[nx*j + i] = y0 + hy*(j - 0.5);
            }
        }
    } else {
        const double hy = (yN - y0)/(ny - 1.0);
        for (uint32_t j = 0; j < ny; j++) {
            for (uint32_t i = 0; i < nx; i++) {
                Y[nx*j + i] = y0 + hy*j;
            }
        }
    }


    fwrite(&nx, 1, sizeof(uint32_t), file);
    fwrite(&ny, 1, sizeof(uint32_t), file);

    fwrite(mem, 1, 2*nx*ny*sizeof(double), file);

    fclose(file);
    free(mem);

    return EXIT_SUCCESS;
}
