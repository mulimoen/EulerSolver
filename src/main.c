#include <errno.h>
#include <float.h> // For DBL_MAX
#include <math.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>


#include "RHS.h"
#include "common.h"
#include "config_reader.h"
#include "metric.h"
#include "operators.h"
#include "timing.h"
#include "vortex.h"
#include "misc.h"

static void get_options_from_args(int argc, char **argv, const char *default_dir, char **dir) {
    const struct option long_options[] = {
        {"help", no_argument, NULL, 'h'},
        {"dir", required_argument, NULL, 'd'},
        {NULL, 0, NULL, 0}
    };

    const char *usage =
        "Usage: euler [options]\n"
        "\n"
        " -h, --help       Show this message and quit.\n"
        " -d, --dir <dir>  Directory to run program from.\n";

    *dir = strdup(default_dir);
    while (true) {
        int option_index = 0;
        const int opt = getopt_long(argc, argv, "hd:", long_options, &option_index);
        if (opt == -1) {
            break;
        }
        switch (opt) {
            case 0: // Some flag is set
                break;
            case 'h':
                free(*dir);
                printf("%s", usage);
                exit(EXIT_SUCCESS);
            case '?': // getopt prints error
                free(*dir);
                fprintf(stderr, "%s", usage);
                exit(EXIT_FAILURE);
            case 'd':
                free(*dir);
                *dir = strdup(optarg);
                break;
            default:
                free(*dir);
                exit(EXIT_FAILURE);
        }
    }

    if (optind < argc) {
        fprintf(stderr, "Unrecognised option(s): ");
        while (optind < argc) {
            fprintf(stderr, "\"%s\" ", argv[optind]);
            optind += 1;
        }
        fprintf(stderr, "\n%s", usage);

        free(*dir);
        exit(EXIT_FAILURE);
    }
}



int main(int argc, char **argv) {
    char *dir;
    get_options_from_args(argc, argv, ".", &dir);

    struct euler_settings settings;
    bool success = read_settings(dir, &settings);
    if (!success) {
        fprintf(stderr, "Could not get settings\n");
        free(dir);
        return EXIT_FAILURE;
    }

    struct SBP_operator op[2];
    get_operator(&op[0], settings.operators[0]);
    get_operator(&op[1], settings.operators[1]);

    struct metric **metrics = malloc(settings.n_grids*sizeof(struct metric *));

    double smallest_h = DBL_MAX;

    for (uint32_t i = 0; i < settings.n_grids; i++) {
        const struct grid grid = settings.grids[i];

        const uint32_t nxi = grid.nx;
        const double hxi = 1.0/(nxi - 1);
        const uint32_t neta = grid.ny;
        const double heta = 1.0/(neta - 1);

        metrics[i] = malloc(nxi*neta*sizeof(struct metric));
        const struct diff_op diff[2] = { *op[0].diff, *op[1].diff };
        compute_metrics(metrics[i], nxi, neta, grid.x, grid.y, diff);

        smallest_h = fmin(smallest_h, hxi);
        smallest_h = fmin(smallest_h, heta);
    }

    double t = 0.0;
    const double gamma = settings.gamma;
    const bool periodic_error = settings.periodic_error;

    struct fields *q[2] = {
        malloc(settings.n_grids*sizeof(struct fields)),
        malloc(settings.n_grids*sizeof(struct fields)),
    }; // Alternating solution
    struct fields *q_analytical = malloc(settings.n_grids*sizeof(struct fields)); // Analytical solution
    for (uint32_t i = 0; i < settings.n_grids; i++) {
        const struct grid grid = settings.grids[i];
        const size_t N = grid.nx*grid.ny;
        q[0][i].rho = malloc(4*N*sizeof(double));
        q[0][i].rhou = &q[0][i].rho[1*N];
        q[0][i].rhov = &q[0][i].rho[2*N];
        q[0][i].e = &q[0][i].rho[3*N];

        q[1][i].rho = malloc(4*N*sizeof(double));
        q[1][i].rhou = &q[1][i].rho[1*N];
        q[1][i].rhov = &q[1][i].rho[2*N];
        q[1][i].e = &q[1][i].rho[3*N];

        q_analytical[i].rho = malloc(4*N*sizeof(double));
        q_analytical[i].rhou = &q_analytical[i].rho[1*N];
        q_analytical[i].rhov = &q_analytical[i].rho[2*N];
        q_analytical[i].e = &q_analytical[i].rho[3*N];

        vortices(q[0][i], grid.x, grid.y, t, grid.nx*grid.ny, settings.v_params, gamma);
    }
    if (periodic_error) {
        copy_fields(q_analytical, q[0], settings.grids, settings.n_grids);
    }

    {
        char *filename = "/initial.bin";
        char *filepath = malloc(1 + strlen(dir) + strlen(filename));
        strcpy(filepath, dir);
        strcat(filepath, filename);
        save_solution_to_file(settings.n_grids, settings.grids, q[0], filepath);
        free(filepath);
    }

    FILE *errorfile;
    {
        char *filename = "/error.txt";
        char *filepath = malloc(1 + strlen(dir) + strlen(filename));
        strcpy(filepath, dir);
        strcat(filepath, filename);

        errorfile = fopen(filepath, "w+e");
        free(filepath);
    }


    const double CFL = 0.2;
    const double T = settings.T;
    const double dt = CFL*smallest_h;

    const uint32_t iter_count = (uint32_t)(T/dt);

    const bool save_continously = settings.save_continously;
    const bool show_progressbar = settings.show_progressbar;
    uint32_t iter_per_cycle[100];
    if (save_continously || show_progressbar) {
        for (uint32_t i = 0; i < 100; i++) {
            iter_per_cycle[i] = iter_count/100 + (i < iter_count%100);
        }
    } else {
        iter_per_cycle[0] = iter_count;
        iter_per_cycle[1] = 0;
    }

    struct fields *qprev = q[1];
    struct fields *qnext = q[0];

    const struct timespec start = getTime();
    for (uint32_t i = 0; i < 100; i++) {
        if (iter_per_cycle[i] == 0) {
            break;
        }
        struct fields *q_swap_tmp = qprev;
        qprev = qnext;
        qnext = q_swap_tmp;
        integrate(qnext, qprev, &t, dt,
                  settings.grids, settings.n_grids,
                  metrics, settings.g_rel,
                  op,
                  gamma, settings.v_params, iter_per_cycle[i]);

        for (uint32_t g = 0; g < settings.n_grids; g++) {
            const struct grid grid = settings.grids[g];
            if (!periodic_error) {
                vortices(q_analytical[g], grid.x, grid.y, t, grid.nx*grid.ny, settings.v_params, gamma);
            }
            const struct h_op h[2] = {*op[0].h, *op[1].h};
            const double err = h_norm(q_analytical[g], qnext[g], grid.nx, grid.ny, h);

            fprintf(errorfile, "%u, %u, %e", grid.nx, grid.ny, err);
            if (g != settings.n_grids - 1) {
                fprintf(errorfile, ", ");
            }
        }
        fprintf(errorfile, "\n");
        fflush(errorfile);

        if (save_continously) {
            char *filename = "/solutionXXX.bin";
            char *filepath = malloc(1 + strlen(dir) + strlen(filename));
            sprintf(filepath, "%s/solution%03d.bin", dir, i+1);

            save_solution_to_file(settings.n_grids, settings.grids, qnext, filepath);
            free(filepath);
        }

        if (show_progressbar) {
            const uint32_t bar_length = 32;
            const uint32_t bar_per = ((i+1)*bar_length)/(100-1);

            printf("\r%3d%% [", i);
            for (uint32_t j = 0; j < bar_per; j++) {
                putchar('*');
            }
            for (uint32_t j = bar_per; j < bar_length; j++) {
                putchar(' ');
            }
            putchar(']');
            fflush(stdout);
        }
    }
    if (show_progressbar) {
        printf("\n");
    }

    const struct timespec end = getTime();
    const double time = deltaTimeSeconds(start, end);

    fclose(errorfile);


    printf("Execution time: %f seconds\n", time);

    if (!save_continously) {
        char *filename = "/solution.bin";
        char *filepath = malloc(1 + strlen(dir) + strlen(filename));
        strcpy(filepath, dir);
        strcat(filepath, filename);

        save_solution_to_file(settings.n_grids, settings.grids, qnext, filepath);
        free(filepath);
    }


    // Reusing q_analytical
    for (uint32_t g = 0; g < settings.n_grids; g++) {
        const struct grid grid = settings.grids[g];
        if (!periodic_error) {
            vortices(q_analytical[g], grid.x, grid.y, t, grid.nx*grid.ny, settings.v_params, gamma);
        }
        for (uint32_t i = 0; i < grid.nx*grid.ny; i++) {
            q_analytical[g].rho[i] -= qnext[g].rho[i];
            q_analytical[g].rhou[i] -= qnext[g].rhou[i];
            q_analytical[g].rhov[i] -= qnext[g].rhov[i];
            q_analytical[g].e[i] -= qnext[g].e[i];
        }
    }

    {
        char *filename = "/error.bin";
        char *filepath = malloc(1 + strlen(dir) + strlen(filename));
        strcpy(filepath, dir);
        strcat(filepath, filename);

        save_solution_to_file(settings.n_grids, settings.grids, q_analytical, filepath);
        free(filepath);
    }


    for (uint32_t i = 0; i < settings.n_grids; i++) {
        free(metrics[i]);
        free(q[0][i].rho);
        free(q[1][i].rho);
        free(q_analytical[i].rho);
    }
    free(metrics);
    free(q[0]);
    free(q[1]);
    free(q_analytical);
    free_settings(&settings);
    free(dir);

    return EXIT_SUCCESS;
}
