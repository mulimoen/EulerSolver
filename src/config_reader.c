#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <errno.h>

#include <libconfig.h>

#include "config_reader.h"

struct grid_parameters {
    char *name;
    char *gridfile;
    char *dir[4]; // E, W, N, S
};

static bool get_grid_relations(const struct grid_parameters *g_params, const uint32_t num_grids, struct grid_relations *g_relations) {
    assert(g_params);
    assert(g_relations);

    for (uint32_t i = 0; i < num_grids; i++) {
        const struct grid_parameters *g_p = &g_params[i];
        for (uint32_t c = 0; c < 4; c++) {
            if (strcmp(g_p->dir[c], "vortex") == 0) {
                g_relations[i].border[c].tag = border_type_vortex;
                continue;
            }
            if (strcmp(g_p->dir[c], "background") == 0) {
                g_relations[i].border[c].tag = border_type_background;
                continue;
            }
            const size_t ident_length = 4;
            const char *gridname = g_p->dir[c] + ident_length;
            if (strncmp(g_p->dir[c], "SAT:", ident_length) == 0) {
                g_relations[i].border[c].tag = border_type_sat;
            } else if (strncmp(g_p->dir[c], "I2F:", ident_length) == 0) {
                g_relations[i].border[c].tag = border_type_interpolation_to_fine;
            } else if (strncmp(g_p->dir[c], "I2C:", ident_length) == 0) {
                g_relations[i].border[c].tag = border_type_interpolation_to_coarse;
            } else {
                fprintf(stderr, "Unknown identifier %s\n", g_p->dir[c]);
                fprintf(stderr, "Expected CON:\"X\" or SAT:\"X\"\n");
                return false;
            }
            bool found_matching = false;
            for (uint32_t j = 0; j < num_grids; j++) {
                if (strcmp(gridname, g_params[j].name) == 0) {
                    g_relations[i].border[c].neighbour = j;
                    found_matching = true;
                    break;
                }
            }
            if (!found_matching) {
                fprintf(stderr, "Could not find a matching grid for grid %d in direction %d\n", i, c);
                return false;
            }
        }
    }
    return true;
}

static bool get_op_name_from_string(const char *name, enum e_operator_names *op) {
    if (strcmp(name, "4") == 0) {
        *op = e_upwind4;
        return true;
    };
    if (strcmp(name, "9") == 0) {
        *op = e_upwind9;
        return true;
    }
    if (strcmp(name, "4 h/2") == 0) {
        *op = e_upwind4_h2;
        return true;
    }
    if (strcmp(name, "9 h/2") == 0) {
        *op = e_upwind9_h2;
        return true;
    }
    if (strcmp(name, "trad 4") == 0) {
        *op = e_traditional4;
        return true;
    }
    if (strcmp(name, "trad 8") == 0) {
        *op = e_traditional8;
        return true;
    }
    return false;
}


static bool get_operator_names(const config_setting_t *operators, enum e_operator_names op_names[2]) {
    for (int i = 0; i < 2; i++) {
        const char *name = config_setting_get_string_elem(operators, i);
        if (!get_op_name_from_string(name, &op_names[i])) {
            fprintf(stderr, "operators[%d] | Could not get operator name\n", i);
            return false;
        }
    }
    return true;
}

// Return true on success, false on failure
static bool get_vortex_settings(config_setting_t *vortex, struct vortex_parameters *v_params) {
    assert(vortex);
    assert(v_params);

    const char *error_type;

    if (!config_setting_lookup_float(vortex, "Mach", &v_params->Mach)) {
        error_type = "Mach";
        goto err;
    }

    config_setting_t *x0 = config_setting_get_member(vortex, "x0");
    if (!x0 || !config_setting_is_array(x0)) {
        fprintf(stderr, "vortex:x0 | not existent or not an array\n");
        return false;
    }

    config_setting_t *y0 = config_setting_get_member(vortex, "y0");
    if (!y0 || !config_setting_is_array(y0)) {
        fprintf(stderr, "vortex:y0 | not existent or not an array\n");
        return false;
    }

    config_setting_t *rstar = config_setting_get_member(vortex, "rstar");
    if (!rstar || !config_setting_is_array(rstar)) {
        fprintf(stderr, "vortex:rstar | not existent or not an array\n");
        return false;
    }

    config_setting_t *epsilon = config_setting_get_member(vortex, "epsilon");
    if (!epsilon || !config_setting_is_array(epsilon)) {
        fprintf(stderr, "vortex:epsilon | not existent or not an array\n");
        return false;
    }

    const int length = config_setting_length(x0);
    if (length != config_setting_length(y0) ||
        length != config_setting_length(rstar) ||
        length != config_setting_length(epsilon)) {
        fprintf(stderr, "vortex: inconsistent lengths\n");
        return false;
    }
    if (length <= 0) {
        fprintf(stderr, "vortex: no elements\n");
        return false;
    }

    v_params->n_vortices = (uint32_t)length;

    double *mem = malloc(4*(size_t)length*sizeof(double));
    v_params->x0 = &mem[0*length];
    v_params->y0 = &mem[1*length];
    v_params->rstar = &mem[2*length];
    v_params->epsilon = &mem[3*length];
    for (int i = 0; i < length; i++) {
        v_params->x0[i] = config_setting_get_float_elem(x0, i);
        v_params->y0[i] = config_setting_get_float_elem(y0, i);
        v_params->rstar[i] = config_setting_get_float_elem(rstar, i);
        v_params->epsilon[i] = config_setting_get_float_elem(epsilon, i);
    }

    return true;
err:
    fprintf(stderr, "vortex:%s | Does not exist/wrong type\n", error_type);
    return false;
}

static bool get_grid_parameters(const config_setting_t *grid, struct grid_parameters *grid_params) {
    assert(grid);
    assert(grid_params);

    const char *name;
    if (!config_setting_lookup_string(grid, "name", &name)) {
        fprintf(stderr, "grids:name | No name found\n");
        goto name;
    }
    grid_params->name = malloc(1 + strlen(name));
    strcpy(grid_params->name, name);

    const char *gridfile;
    if (!config_setting_lookup_string(grid, "gridfile", &gridfile)) {
        fprintf(stderr, "grids:gridfile | No gridfile option\n");
        goto gridfile;
    }
    grid_params->gridfile = malloc(1 + strlen(gridfile));
    strcpy(grid_params->gridfile, gridfile);

    const char *dirW;
    if (!config_setting_lookup_string(grid, "dirW", &dirW)) {
        fprintf(stderr, "grids:dirW | No option found\n");
        goto dirW;
    }
    grid_params->dir[w_border] = malloc(1 + strlen(dirW));
    strcpy(grid_params->dir[w_border], dirW);

    const char *dirE;
    if (!config_setting_lookup_string(grid, "dirE", &dirE)) {
        fprintf(stderr, "grids:dirE | No option found\n");
        goto dirE;
    }
    grid_params->dir[e_border] = malloc(1 + strlen(dirE));
    strcpy(grid_params->dir[e_border], dirE);

    const char *dirN;
    if (!config_setting_lookup_string(grid, "dirN", &dirN)) {
        fprintf(stderr, "grids:dirN | No option found\n");
        goto dirN;
    }
    grid_params->dir[n_border] = malloc(1 + strlen(dirN));
    strcpy(grid_params->dir[n_border], dirN);

    const char *dirS;
    if (!config_setting_lookup_string(grid, "dirS", &dirS)) {
        fprintf(stderr, "grids:dirS | No option found\n");
        goto dirS;
    }
    grid_params->dir[s_border] = malloc(1 + strlen(dirS));
    strcpy(grid_params->dir[s_border], dirS);

    return true;

dirS:
    free(grid_params->dir[n_border]);
dirN:
    free(grid_params->dir[e_border]);
dirE:
    free(grid_params->dir[w_border]);
dirW:
    free(grid_params->gridfile);
gridfile:
    free(grid_params->name);
name:
    return false;
}

static void free_grid_param(struct grid_parameters *g_params) {
    assert(g_params);

    free(g_params->name);
    free(g_params->gridfile);
    for (uint32_t i = 0; i < 4; i++) {
        free(g_params->dir[i]);
    }
}

static void free_parameters(struct grid_parameters **g_params, const uint32_t length) {
    assert(g_params);

    for (uint32_t i = 0; i < length; i++) {
        free_grid_param(&(*g_params)[i]);
    }

    free(*g_params);
}

static bool get_raw_settings(const char *filename, double *gamma, struct vortex_parameters *v_params, struct grid_parameters **g_params, uint32_t *num_grids, double *T, enum e_operator_names op_names[2], bool *save_cont, bool *show_progress, bool *periodic_error) {
    assert(filename);
    assert(gamma);
    assert(v_params);
    assert(g_params);
    assert(num_grids);

    config_t cfg;
    config_init(&cfg);

    config_set_options(&cfg,
            CONFIG_OPTION_ALLOW_SCIENTIFIC_NOTATION |
            CONFIG_OPTION_AUTOCONVERT);

    errno = 0;
    FILE *file = fopen(filename, "re");
    if (!file) {
        fprintf(stderr, "File \"%s\" could not be opened: %s\n", filename, strerror(errno));
        fprintf(stderr, "Folders can be specified with an argument, see help text\n");
        return false;
    }

    bool success = config_read(&cfg, file);
    fclose(file);
    if (!success) {
        fprintf(stderr, "line %d - %s\n",
config_error_line(&cfg), config_error_text(&cfg));
        config_destroy(&cfg);
        return false;
    }

    config_setting_t *root = config_root_setting(&cfg);

    int exists = config_setting_lookup_float(root, "gamma", gamma);
    if (!exists) {
        fprintf(stderr, "gamma | Does not exist/wrong type\n");
        config_destroy(&cfg);
        return false;
    }
    exists = config_setting_lookup_float(root, "T", T);
    if (!exists) {
        fprintf(stderr, "T | Does not exist/wrong type\n");
        config_destroy(&cfg);
        return false;
    }

    int sav_cont;
    exists = config_setting_lookup_bool(root, "save_continously", &sav_cont);
    if (exists) {
        *save_cont = sav_cont;
    } else {
        *save_cont = false;
    }

    int show_bar;
    exists = config_setting_lookup_bool(root, "progressbar", &show_bar);
    if (exists) {
        *show_progress = show_bar;
    } else {
        *show_progress = false;
    }

    int periodic_sol;
    exists = config_setting_lookup_bool(root, "periodic_solution", &periodic_sol);
    if (exists) {
        *periodic_error = periodic_sol;
    } else {
        *periodic_error = false;
    }


    config_setting_t *operators = config_setting_get_member(root, "operators");
    if (!operators) {
        fprintf(stderr, "operators | Option does not exist\n");
        config_destroy(&cfg);
        return false;
    }
    bool operator_exist = get_operator_names(operators, op_names);
    if (!operator_exist) {
        fprintf(stderr, "operators | Invalid member\n");
        config_destroy(&cfg);
        return false;
    }

    config_setting_t *vortex = config_setting_get_member(root, "vortex");
    if (!vortex) {
        fprintf(stderr, "vortex | Option does not exist\n");
        config_destroy(&cfg);
        return false;
    }
    bool vortex_exist = get_vortex_settings(vortex, v_params);
    if (!vortex_exist) {
        fprintf(stderr, "vortex | Could not read option\n");
        config_destroy(&cfg);
        return false;
    }

    config_setting_t *grids = config_setting_get_member(root, "grids");
    if (!grids) {
        fprintf(stderr, "grids | Option does not exist\n");
        config_destroy(&cfg);
        return false;
    }

    int length = config_setting_length(grids);
    if (length < 1) {
        fprintf(stderr, "grids | Must contain at least one grid\n");
        config_destroy(&cfg);
        return false;
    }

    *g_params = malloc((size_t)length*sizeof(struct grid_parameters));

    *num_grids = (uint32_t)length;
    for (uint32_t i = 0; i < *num_grids; i++) {
        config_setting_t *grid = config_setting_get_elem(grids, i);
        assert(grid);

        success = get_grid_parameters(grid, &(*g_params)[i]);
        if (!success) {
            fprintf(stderr, "grids[%u] | Invalid config\n", i);
            free_parameters(g_params, i);
            config_destroy(&cfg);
            return false;
        }
    }

    config_destroy(&cfg);

    return true;
}

static bool read_grid_file(const char *filename, double **x, double **y, uint32_t *nx, uint32_t *ny) {
    assert(filename);
    assert(x);
    assert(y);
    assert(nx);
    assert(ny);

    errno = 0;
    FILE *file = fopen(filename, "re");
    if (!file) {
        fprintf(stderr, "File \"%s\" could not be opened: %s\n", filename, strerror(errno));
        return false;
    }

    fread(nx, 1, sizeof(uint32_t), file);
    fread(ny, 1, sizeof(uint32_t), file);

    const uint32_t N = (*nx)*(*ny);
    *x = malloc(N*sizeof(double));
    *y = malloc(N*sizeof(double));

    fread(*x, 1, N*sizeof(double), file);
    fread(*y, 1, N*sizeof(double), file);

    fclose(file);
    return true;
}

bool read_settings(const char *path, struct euler_settings *settings) {
    if (!path || !settings) {
        fprintf(stderr, "read_settings() received NULL\n");
        return false;
    }

    const char *config_ending = "/setup.cfg";
    const size_t pathlength = strlen(path);
    char *filename = malloc(1 + pathlength + strlen(config_ending));

    strcpy(filename, path);
    strcat(filename, config_ending);

    struct grid_parameters *g_params = NULL;

    bool success = get_raw_settings(filename, &settings->gamma, &settings->v_params, &g_params, &settings->n_grids, &settings->T, settings->operators, &settings->save_continously, &settings->show_progressbar, &settings->periodic_error);
    free(filename);
    if (!success) {
        fprintf(stderr, "Error parsing options\n");
        return false;
    }


    settings->g_rel = malloc(settings->n_grids*sizeof(struct grid_relations));
    success = get_grid_relations(g_params, settings->n_grids, settings->g_rel);
    if (!success) {
        fprintf(stderr, "Error getting relations between the grids\n");
        free(settings->g_rel);
        free_parameters(&g_params, settings->n_grids);
        return false;
    }

    settings->grids = malloc(settings->n_grids*sizeof(struct grid));
    for (uint32_t i = 0; i < settings->n_grids; i++) {
        filename = malloc(1 + pathlength + 1 + strlen(g_params[i].gridfile));
        strcpy(filename, path);
        strcat(filename, "/");
        strcat(filename, g_params[i].gridfile);

        struct grid *g = &(settings->grids[i]);

        success = read_grid_file(filename, &g->x, &g->y, &g->nx, &g->ny);
        free(filename);

        if (!success) {
            free_parameters(&g_params, settings->n_grids);
            free(settings->grids);
            free(settings->g_rel);
            fprintf(stderr, "Error reading grid file\n");
            return false;
        }

    }

    free_parameters(&g_params, settings->n_grids);

    return true;
}

void free_settings(struct euler_settings *settings) {
    for (uint32_t i = 0; i < settings->n_grids; i++) {
        free(settings->grids[i].x);
        free(settings->grids[i].y);
    }

    free(settings->grids);
    free(settings->g_rel);
    free(settings->v_params.x0);
}
