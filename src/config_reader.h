#pragma once

#include <stdbool.h>
#include <stdint.h>

#include "common.h"
#include "vortex.h"
#include "operators.h"

struct euler_settings {
    struct grid_relations *g_rel;
    struct grid *grids;
    uint32_t n_grids;

    bool save_continously; // Default false
    bool show_progressbar; // Default false
    bool periodic_error; // Default false
    char pad_;

    double gamma;
    struct vortex_parameters v_params;

    double T;

    enum e_operator_names operators[2];
};


// Returns true on success, false on failure
// settings will need to be free by free_settings()
// on success
bool read_settings(const char *path, struct euler_settings *settings);

void free_settings(struct euler_settings *settings);

/* Basis of setup.cfg

gamma = 1.4

grids = (
    {
        name = "grid"
        gridfile = "grid.xy"
        dirE = "SAT:other"
        dirW = "vortex"
        dirN = "background"
        dirS = "I2F:other" // Interpolates "other" to fine
    },
    {
        name = "other"
        ...
    }
      )


vortex = {
    Mach = 0.5
    x0 = [-1, -2]
    y0 = [0.0, 0.0]
    rstar = [0.5, 2.0]
    epsilon = [1.0, 0.1]
    }

T = 2

operators = ["9", "9 h/2"]

// The following options are optional
save_continously = True // Saves a snapshot of the solution 100 times
progressbar = False
periodic_solution = False // If the solution is fully periodic every time it is computed (every 100 times)

 */
