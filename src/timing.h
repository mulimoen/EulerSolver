#include <time.h>


static inline struct timespec getTime(void) {
    struct timespec t;
    clock_gettime(CLOCK_MONOTONIC, &t);
    return t;
}

static inline double deltaTimeSeconds(const struct timespec start,
                                      const struct timespec end) {
    return (end.tv_sec - start.tv_sec) + (end.tv_nsec - start.tv_nsec)*1e-9;
}
