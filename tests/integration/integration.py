#! /usr/bin/env python3

import argparse
import os
import tempfile
import shutil
import sys
import subprocess

SIZE = 20


def euler_single(euler, gridgen, working_dir):
    shutil.copyfile("single.cfg", os.path.join(working_dir, "setup.cfg"))
    print("Creating grid")
    subprocess.run([gridgen, str(SIZE), str(SIZE), "-5", "5", "-5", "5",
                    os.path.join(working_dir, "grid.xy")],
                   stdout=subprocess.DEVNULL)

    print("Running single")
    subprocess.run([euler, "-d", working_dir])

    area = 10*10
    with open(os.path.join(working_dir, "error.txt")) as f:
        line = f.readline()
        error = area*float(line.split(',')[2])

    return error


def euler_double(euler, gridgen, working_dir):
    shutil.copyfile("double.cfg", os.path.join(working_dir, "setup.cfg"))
    print("Creating grid")
    subprocess.run([gridgen, str(SIZE), str(SIZE//2), "-5", "5", "-5", "0",
                    os.path.join(working_dir, "grid0.xy")],
                   stdout=subprocess.DEVNULL)
    subprocess.run([gridgen, str(SIZE), str(SIZE//2), "-5", "5", "0", "5",
                    os.path.join(working_dir, "grid1.xy")],
                   stdout=subprocess.DEVNULL)

    print("Running double")
    subprocess.run([euler, "-d", working_dir])

    area = 10*5
    with open(os.path.join(working_dir, "error.txt")) as f:
        line = f.readline().split(',')
        error = area*float(line[2]) + area*float(line[5])

    return error


def euler_interpol(euler, gridgen, working_dir):
    shutil.copyfile("interpol.cfg", os.path.join(working_dir, "setup.cfg"))
    print("Creating grid")
    subprocess.run([gridgen, str(2*SIZE-1), str(SIZE), "-5", "5", "-5", "0",
                    os.path.join(working_dir, "grid0.xy")],
                   stdout=subprocess.DEVNULL)
    subprocess.run([gridgen, str(SIZE), str(SIZE//2), "-5", "5", "0", "5",
                    os.path.join(working_dir, "grid1.xy")],
                   stdout=subprocess.DEVNULL)

    print("Running interpolation")
    subprocess.run([euler, "-d", working_dir])

    area = 10*5
    with open(os.path.join(working_dir, "error.txt")) as f:
        line = f.readline().split(',')
        error = area*float(line[2]) + area*float(line[5])

    return error


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--euler", type=str, default="euler",
                        help="path to euler executable")
    parser.add_argument("--gridgen", type=str, default="gridgenerator",
                        help="path to gridgenerator")

    args = parser.parse_args()
    euler = args.euler
    gridgen = args.gridgen

    print(euler)
    print(gridgen)

    failure_occured = False
    with tempfile.TemporaryDirectory() as working_dir:
        single_dir = os.path.join(working_dir, "single")
        os.mkdir(single_dir)
        err_single = euler_single(euler, gridgen, single_dir)
        err_single_expect = 0.75
        if err_single > err_single_expect:
            print("Single: Error {} is larger than expected {}".format(
                    err_single, err_single_expect), file=sys.stderr)
            failure_occured = True

        double_dir = os.path.join(working_dir, "double")
        os.mkdir(double_dir)
        err_double = euler_double(euler, gridgen, double_dir)
        err_double_expect = 2.6
        if err_double > err_double_expect:
            print("Double: Error {} is larger than expected {}".format(
                    err_double, err_double_expect), file=sys.stderr)
            failure_occured = True

        interpol_dir = os.path.join(working_dir, "interpol")
        os.mkdir(interpol_dir)
        err_interpol = euler_interpol(euler, gridgen, interpol_dir)
        err_interpol_expect = 2.1
        if err_interpol > err_interpol_expect:
            print("Interpolation: Error {} is larger than expected {}".format(
                    err_interpol, err_interpol_expect), file=sys.stderr)
            failure_occured = True

    exit(failure_occured)
